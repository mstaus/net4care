package org.net4care.clinicianlogin;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.w3c.dom.Document;

import dk.nsi.nsp.guides.common.security.CredentialVaultWrapper;
import dk.sosi.seal.SOSIFactory;
import dk.sosi.seal.model.AuthenticationLevel;
import dk.sosi.seal.model.CareProvider;
import dk.sosi.seal.model.SecurityTokenRequest;
import dk.sosi.seal.model.SecurityTokenResponse;
import dk.sosi.seal.model.SignatureConfiguration;
import dk.sosi.seal.model.SignatureUtil;
import dk.sosi.seal.model.SystemIDCard;
import dk.sosi.seal.model.UserIDCard;
import dk.sosi.seal.model.UserInfo;
import dk.sosi.seal.model.constants.IDValues;
import dk.sosi.seal.pki.SOSITestFederation;
import dk.sosi.seal.pki.SignatureProviderFactory;
import dk.sosi.seal.vault.CredentialVault;
import dk.sosi.seal.xml.XmlUtil;


public class IDCardProvider {

	private String alias_system = "SOSI:ALIAS_SYSTEM"; 
	//Is the alias only for the example keystores or is this also true for general certificates
	private static final String idcard_username = null;
	private static final String idcard_password = null;

	private final String sts_endpoint;
	
	private String org_type = null;
	private String org_id = null;
	private String org_name = null;
	private String systemName = null;
	
	public IDCardProvider(String endpoint){
		this.sts_endpoint = endpoint;
	}
		
	public String getAliasSystem(){
		return this.alias_system;
	}
	
	public void setAliasSystem(String aliasSystem){
		this.alias_system = aliasSystem;
	}
	
	public void setOrganization(String orgId, String orgName, String orgType){
		this.org_id = orgId;
		this.org_name = orgName;
		if(orgType != null){
			this.org_type = orgType;	
		}
	}

	public void setSystemName(String name){
		this.systemName = name;
	}

	/**
	 * Produces a selfsigned IDCard which is then sent to the STS for signing.
	 * The IDCard signed by the STS is returned.
	 * @param vault The CredentialVault containing the MOCES certificate of the user
	 * @param userInfo UserInfo describing the user
	 * @return a UserIDCard signed by the STS
	 * @throws IOException
	 */
	public UserIDCard getUserIDCard(CredentialVault vault, UserInfo userInfo) throws IOException{
		SOSIFactory sosiFactory = createSOSIFactory(vault);

		CareProvider careProvider = new CareProvider(org_type, org_id, org_name);
		UserIDCard selfSignedUserIdCard = sosiFactory.createNewUserIDCard(systemName, userInfo, careProvider, AuthenticationLevel.MOCES_TRUSTED_USER, idcard_username, idcard_password, vault.getSystemCredentialPair().getCertificate(), null);

		SecurityTokenRequest securityTokenRequest = sosiFactory.createNewSecurityTokenRequest();
		securityTokenRequest.setIDCard(selfSignedUserIdCard);
		Document doc = securityTokenRequest.serialize2DOMDocument();
		
		SignatureConfiguration signatureConfiguration = new SignatureConfiguration(new String[] { IDValues.IDCARD }, IDValues.IDCARD, IDValues.id);
		SignatureUtil.sign(SignatureProviderFactory.fromCredentialVault(vault), doc, signatureConfiguration);

		String requestXml = XmlUtil.node2String(doc, false, true);

		String responseXml = sendRequest(sts_endpoint, "Issue", requestXml);

		SecurityTokenResponse securityTokenResponse = sosiFactory.deserializeSecurityTokenResponse(responseXml);

		UserIDCard stsSignedIdCard = (UserIDCard) securityTokenResponse.getIDCard();
		if (securityTokenResponse.isFault() || stsSignedIdCard == null) {
			System.err.println("We got a SOAP Fault from STS:\n"+responseXml);
			return null;
		}
		return stsSignedIdCard;
	}
	
	/**
	 * Produces a selfsigned IDCard which is then sent to the STS for signing.
	 * The IDCard signed by the STS is returned.
	 * @param path The path for a Java Keystore containing the MOCES certificate of the user
	 * @param keyStorePassword The password for the keystore
	 * @param userInfo UserInfo describing the user
	 * @return a UserIDCard signed by the STS
	 * @throws IOException
	 */
	public UserIDCard getUserIDCard(String keyStorePath, String keyStorePassword, UserInfo userInfo) throws IOException {
		CredentialVault vault = getVaultFromFile(keyStorePath, keyStorePassword, alias_system);
		
		return this.getUserIDCard(vault, userInfo);
	}
	
	/**
	 * Produces a IDCard which is then sent to the STS for signing.
	 * The IDCard signed by the STS is returned.
	 * @param vault A @CredentialVault containing the VOCES or FOCES certificate of the system.
	 * @param keyStorePassword The password for the keystore
	 * @return a SystemIDCard signed by the STS
	 * @throws IOException
	 */
	public SystemIDCard getSystemIDCard(CredentialVault vault) throws IOException{
		SOSIFactory sosiFactory = createSOSIFactory(vault);

		CareProvider careProvider = new CareProvider(org_type, org_id, org_name);
		SystemIDCard systemIdCard = sosiFactory.createNewSystemIDCard(systemName, careProvider, AuthenticationLevel.VOCES_TRUSTED_SYSTEM, idcard_username, idcard_password, vault.getSystemCredentialPair().getCertificate(), null);
				
		SecurityTokenRequest securityTokenRequest = sosiFactory.createNewSecurityTokenRequest();
		securityTokenRequest.setIDCard(systemIdCard);
		Document doc = securityTokenRequest.serialize2DOMDocument();

		String requestXml = XmlUtil.node2String(doc, false, true);

		String responseXml = sendRequest(sts_endpoint, "Issue", requestXml);

		SecurityTokenResponse securityTokenResponse = sosiFactory.deserializeSecurityTokenResponse(responseXml);

		SystemIDCard stsSignedIdCard = (SystemIDCard) securityTokenResponse.getIDCard();
		if (securityTokenResponse.isFault() || stsSignedIdCard == null) {
			System.err.println("We got a SOAP Fault from STS:\n"+responseXml);
			return null;
		}
		return stsSignedIdCard;
	}

	/**
	 * Produces a IDCard which is then sent to the STS for signing.
	 * The IDCard signed by the STS is returned.
	 * @param path The path for a Java Keystore containing the VOCES or FOCES certificate of the system.
	 * @param keyStorePassword The password for the keystore
	 * @return a SystemIDCard signed by the STS
	 * @throws IOException
	 */
	public SystemIDCard getSystemIDCard(String keyStorePath, String keyStorePassword) throws IOException {
		CredentialVault vault = getVaultFromFile(keyStorePath, keyStorePassword, alias_system);
		return this.getSystemIDCard(vault);
	}

	
	/**
	 * Build a certificate vault capable of providing credential pairs containing the certificate and private key that
	 * will be used to sign the idcard. In this example the certificate and private key are simply read from a Java
	 * keystore file and wrapped in a simple credential vault. Real systems will sometimes need to create there own
	 * implementations of the {@link CredentialVault} interface that provides credential pairs based on what ever source
	 * is available
	 */
	private CredentialVault getVaultFromFile(String path, String password, String alias) throws IOException {
		try {
			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
			if(in == null) {
				in = new FileInputStream(path);
			}
			
			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(in, password.toCharArray());
			X509Certificate certificate = (X509Certificate) keyStore.getCertificate(alias);
			PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, password.toCharArray());
			return new CredentialVaultWrapper(certificate, privateKey);
		} catch (KeyStoreException e) {
			throw new IOException("Error loading certificate and private key from " + path, e);
		} catch (NoSuchAlgorithmException e) {
			throw new IOException("Error loading certificate and private key from " + path, e);
		} catch (CertificateException e) {
			throw new IOException("Error loading certificate and private key from " + path, e);
		} catch (UnrecoverableKeyException e) {
			throw new IOException("Error loading certificate and private key from " + path, e);
		}
	}

	/**
	 * Builds a SOSI factory that will validate all documents created. The factory is configured to work in the test
	 * federation.
	 */
	private SOSIFactory createSOSIFactory(CredentialVault vault) {
		Properties props = new Properties(System.getProperties());
		props.setProperty(SOSIFactory.PROPERTYNAME_SOSI_VALIDATE, Boolean.toString(true));
		SOSIFactory sosiFactory = new SOSIFactory(new SOSITestFederation(new Properties()), vault, props);
		return sosiFactory;
	}

	private String sendRequest(String url, String soapAction, String postBody) throws IOException {
		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		defaultHttpClient.getParams().setParameter("http.connection.timeout", 30000);

		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader(new BasicHeader("Content-Type", "text/xml; charset=utf-8;"));
		httpPost.addHeader(new BasicHeader("SOAPAction", "\"" + soapAction + "\""));
		httpPost.addHeader(new BasicHeader("X-NSI-GW-PARTITION", "test")); //TODO ??
		httpPost.setEntity(new StringEntity(postBody, Charset.forName("utf-8")));

		HttpResponse response = defaultHttpClient.execute(httpPost);

		String result = "";
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode == 200 || statusCode == 500) {
			InputStream is = null;
			try {
				is = response.getEntity().getContent();
				result = IOUtils.toString(is, Charset.forName("utf-8"));
			} finally {
				if (is != null) {
					is.close();
				}
			}
		} else if (statusCode == 202) {
			defaultHttpClient.close();
			return ""; //Accepted without any response
		} else {
			result = response.getStatusLine().getReasonPhrase();
			defaultHttpClient.close();
			throw new IOException("HTTP POST failed (" + statusCode + "): " + result);
		}
		defaultHttpClient.close();
		return result;
	}

	
}
