package org.net4care.clinicianlogin;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;
import org.junit.Test;
import org.net4care.clinicianlogin.IDCardProvider;

import dk.sosi.seal.model.SystemIDCard;
import dk.sosi.seal.model.UserIDCard;
import dk.sosi.seal.model.UserInfo;
import dk.sosi.seal.model.constants.SubjectIdentifierTypeValues;

public class IDCardProviderTest {

	@Test
	public void testUserIDCard() throws IOException{
		String keystorePath = "Karl_Hoffmann_Svendsen_Laege.jks";
		String keystore_password = "!234Qwer";
	
		String user_cpr = "0102732379";
		String user_given_name = "Karl Hoffmann";
		String user_sur_name = "Svendsen";
		String user_email = "Karl_Hoffmann_Svendsen@nsi.dk";
		String user_occupation = "Læge";
		String user_education_code = "IGNORED"; 
		// Must not be an empty string or null, but all values that are not four digits are ignored by STS
		String user_authorization_code = null;
			
		UserInfo userInfo = new UserInfo(user_cpr, user_given_name, user_sur_name, user_email, user_occupation, user_education_code, user_authorization_code);
			
		IDCardProvider idcardProvider = getDefaultTestProvider();
		
		UserIDCard card = idcardProvider.getUserIDCard(keystorePath, keystore_password, userInfo);
		
		assertNotNull(card.getSignedByCertificate());
		assertEquals(userInfo.getGivenName(), card.getUserInfo().getGivenName());
		Date today = new Date();
		assertTrue(card.getExpiryDate().after(today));
		
	}
	
	@Test
	public void testVOCESCertificate() throws IOException{
		
		IDCardProvider idcardProvider = getDefaultTestProvider();
			
		String keystorePath = "Statens_Serum_Institut_VOCES.jks";
		String keystore_password = "!234Qwer";
	
		SystemIDCard card = idcardProvider.getSystemIDCard(keystorePath, keystore_password);
		
		assertNotNull(card.getSignedByCertificate());
		Date today = new Date();
		assertTrue(card.getExpiryDate().after(today));
		
	}
	
	@Test
	public void testFOCESCerficate() throws IOException{
		
		IDCardProvider idcardProvider = getDefaultTestProvider();
		String keystorePath = "Statens_Serum_Institut_FOCES.jks";
		String keystore_password = "!234Qwer";
	
		SystemIDCard card = idcardProvider.getSystemIDCard(keystorePath, keystore_password);
		
		assertNotNull(card.getSignedByCertificate());
		Date today = new Date();
		assertTrue(card.getExpiryDate().after(today));
	}
	
	private IDCardProvider getDefaultTestProvider(){
		IDCardProvider idcardProvider = new IDCardProvider("http://test1.ekstern-test.nspop.dk:8080/sts/services/NewSecurityTokenService");
		idcardProvider.setOrganization("46837428", "Statens Serum Institut", SubjectIdentifierTypeValues.CVR_NUMBER);
		idcardProvider.setSystemName("IDCardProvider test system");
		return idcardProvider;
	}
}
