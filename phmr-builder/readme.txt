PREREQUISITES
- Java version 1.6
- Maven version 3.0

BUILD
- Clone project from: https://bitbucket.org/4s/net4care/src
- Go into phmr-builder folder
- Execute: mvn install

DEVELOP
- Execute: mvn eclipse:eclipse
- Import project into eclipse, see http://help.eclipse.org/kepler/index.jsp?topic=/org.eclipse.platform.doc.user/tasks/tasks-importproject.htm
- Set workspace encoding to UTF-8 and tab to spaces and size to 2

DEPLOY
- Add artifactory credentials to $HOME/.m2/settings.xml
- Execute: mvn deploy
