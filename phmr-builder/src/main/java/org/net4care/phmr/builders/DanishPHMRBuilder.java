package org.net4care.phmr.builders;

import java.text.*;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.*;

import org.net4care.phmr.codes.HL7;
import org.net4care.phmr.codes.Loinc;
import org.net4care.phmr.codes.MedCom;
import org.net4care.phmr.codes.NSI;
import org.net4care.phmr.model.*;
import org.w3c.dom.*;

/**
 * An implementation of the PhmrBuilder that can build
 * an XML Document object following the Danish
 * PHMR localization.
 *
 * To use you should:
 *
 * a) Create a SimpleClinicalDocument instance, i
 * b) Instantiate an instance of this class, b
 * c) invoke i.construct(b);
 * d) the document can be retrieved using b.getDocument()
 *
 * Implementation note: Raw DOM classes are used as we
 * did not find proper xsd files to generate java classes
 * using JAXB.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 */

public final class DanishPHMRBuilder implements PhmrBuilder {

  private Document result;
  private Element root;
  private String uniqueId;
  private Element structuredBody;
  private UUIDStrategy uuidStrategy;

  /** dateTimeformatter that can translate into the HL7 formatting. */ 
  private static Format dateTimeformatter = new SimpleDateFormat("yyyyMMddHHmmssZZZZ"); 
  private static Format dateFormatter = new SimpleDateFormat("yyyyMMdd"); 

  /** Construct a builder that can produce a java XML document
   * containing a correctly formatted Danish PHMR.
   * 
   * @param uuidStrategy the strategy to generate UUID for
   * the document. You should normally use a 'StandardUUIDStrategy'
   * instance which will generate a new random UUID.
   */
  public DanishPHMRBuilder(UUIDStrategy uuidStrategy) {
    this.uuidStrategy = uuidStrategy;

    // TODO: Ensure UTF-8 encoding

    // Create the XML document... 
    DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance(); 
    DocumentBuilder docBuilder = null; 
    try { 
      docBuilder = dbfac.newDocumentBuilder(); 
    } catch (ParserConfigurationException e) { 
      // TODO logger.error("Error in Parser configuration", e); 
      e.printStackTrace();
    }
    result = docBuilder.newDocument();
  }

  /** Get the XML document.
   * @return the XML document.
   */
  public Document getDocument() {
    return result;
  }

  @Override
  public void buildRootNode() {
    // CONF-PHMR-1: 
    root = result.createElement("ClinicalDocument"); 
    root.setAttribute("xmlns", "urn:hl7-org:v3");
    root.setAttribute("classCode", "DOCCLIN");
    root.setAttribute("moodCode", "EVN");
    // Required by the apache serializer for handling xsi 
    root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

    result.appendChild(root);
  }

  @Override
  public void buildHeader() {
    // The sequencing is important of the following tags (and not 
    // quite what is defined in the PHMR report!), refer to 
    // Boone chapter 14. 

    // CONF-PHMR-? - SECTION 2.6 
    Element typeId = getDocument().createElement("typeId"); 
    typeId.setAttribute("root", HL7.PHMR_TYPEID_ROOT); 
    typeId.setAttribute("extension", HL7.PHMR_DK_TYPEID_EXTENSION); 
    root.appendChild(typeId); 

    // CONF-DK PHMR-2 
    Element templateId = getDocument().createElement("templateId"); 
    templateId.setAttribute("root", MedCom.DK_PHMR_ROOT_POD); 
    root.appendChild(templateId); 

    // CONF-PHMR-DK-19
    uniqueId = uuidStrategy.generate();

    Element id = createId(MedCom.ROOT_OID, MedCom.ROOT_AUTHORITYNAME, uniqueId);
    root.appendChild(id); 

    // CONF-PHMR-3:
    Element code = getDocument().createElement("code"); 
    code.setAttribute("code", Loinc.PHMR_CODE); 
    code.setAttribute("displayName", Loinc.PMHR_DISPLAYNAME); 

    code.setAttribute("codeSystem", Loinc.OID); 
    code.setAttribute("codeSystemName", Loinc.DISPLAYNAME); 
    root.appendChild(code); 
  }

  @Override
  public void buildContext(PersonIdentity patientIdentity, Date effectiveTime, 
      String setId, String versionNumber) {
    // CONF-PHMR-15 
    Element title = getDocument().createElement("title"); 
    title.setTextContent("Hjemmemonitorering for " + patientIdentity.getSSN()); 
    root.appendChild(title); 

    // CONF-PHMR-16 / CONF-DK PHMR-17
    appendEffectiveTime(root, effectiveTime);

    // Section 2.9 
    Element confidentialitycode = getDocument().createElement("confidentialityCode"); 
    confidentialitycode.setAttribute("code", "N"); 
    confidentialitycode.setAttribute("codeSystem", HL7.CONFIDENTIALITY_OID); 
    root.appendChild(confidentialitycode); 

    // CONF-PHMR-17-20 
    Element languageCode = getDocument().createElement("languageCode"); 
    languageCode.setAttribute("code", "da-DK"); 
    root.appendChild(languageCode);

    // CONF-DK PHMR-22
    Element setIdElement = getDocument().createElement("setId"); 
    setIdElement.setAttribute("root", MedCom.MESSAGECODE_OID); 
    setIdElement.setAttribute("extension", setId);
    root.appendChild(setIdElement); 

    Element versionNumberElement = getDocument().createElement("versionNumber"); 
    versionNumberElement.setAttribute("value", versionNumber);
    root.appendChild(versionNumberElement); 

  }

  private void appendEffectiveTime(Element root2, Date effectiveTime) {
    Element effectiveTimeElement = getDocument().createElement("effectiveTime"); 
    String observationTimeAsHL7String = dateTimeformatter.format(effectiveTime); 
    effectiveTimeElement.setAttribute("value", observationTimeAsHL7String); 
    root2.appendChild(effectiveTimeElement); 
  }

  private Element createId(String root, String authorityName, String extension) { 
    Element id = getDocument().createElement("id"); 
    id.setAttribute("assigningAuthorityName", authorityName);
    id.setAttribute("extension", extension); 
    id.setAttribute("root", root); 
    return id; 
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {

    // CONF-PHMR-24-28 
    Element recordTarget = getDocument().createElement("recordTarget");
    recordTarget.setAttribute("typeCode", "RCT"); 
    recordTarget.setAttribute("contextControlCode", "OP"); 
    root.appendChild(recordTarget);

    Element patientRole = getDocument().createElement("patientRole");
    patientRole.setAttribute("classCode", "PAT"); 

    // Decision - we use CPR number as ID under the codeSystem 
    // of Danish CPR 
    Element id = createId(MedCom.CPR_OID, MedCom.CPR_AUTHORITYNAME, patientIdentity.getSSN()); 
    patientRole.appendChild(id); 

    // ADDRESS 
    // CONF-PHMR-5 
    Element addr = generateAddr(patientIdentity.getAddress()); 
    patientRole.appendChild(addr); 

    Element[] telecom = generateTelecom(patientIdentity.getTelecomList()); 
    for (int i = 0; i < telecom.length; i++) {
      patientRole.appendChild(telecom[i]); 
    }
    // PATIENT 

    // The CDA book p 161 
    // CONF-PHMR-4 
    Element patient = getDocument().createElement("patient");
    patient.setAttribute("classCode", "PSN"); 
    patient.setAttribute("determinerCode", "INSTANCE"); 

    Element name = generateName(patientIdentity); 
    patient.appendChild(name); 

    String gender;
    switch (patientIdentity.getGender()) {
    case Female:
      gender = "F";
      break;
    case Male:
      gender = "M";
      break;
    default: // Undifferentiated
      gender = "UN";
      break;
    }

    Element administrativeGenderCode = getDocument().createElement("administrativeGenderCode"); 
    administrativeGenderCode.setAttribute("code", gender); 
    administrativeGenderCode.setAttribute("codeSystem", HL7.GENDER_OID);
    patient.appendChild(administrativeGenderCode); 

    Element birthTime = getDocument().createElement("birthTime"); 
    birthTime.setAttribute("value", dateFormatter.format(patientIdentity.getBirthTime())); 
    patient.appendChild(birthTime); 

    patientRole.appendChild(patient); 

    recordTarget.appendChild(patientRole); 
  }

  @Override
  public void buildAuthorSection(OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStartTime) {

    // Section 2.13.2 

    // The CDA book p 151 
    Element author = getDocument().createElement("author");
    author.setAttribute("typeCode", "AUT"); 
    author.setAttribute("contextControlCode", "OP");
    root.appendChild(author); 

    Element time = getDocument().createElement("time"); 
    time.setAttribute("value", dateTimeformatter.format(authorParticipationStartTime)); 
    author.appendChild(time); 

    Element assignedAuthor = getDocument().createElement("assignedAuthor");
    assignedAuthor.setAttribute("classCode", "ASSIGNED");
    author.appendChild(assignedAuthor); 

    // TODO: Refactor to remove code duplicated from patient section.
    Element id = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, authorOrganizationIdentity.getOrgOID());
    assignedAuthor.appendChild(id); 

    // ADDRESS 
    // CONF-PHMR-5 
    Element addr = generateAddr(authorOrganizationIdentity.getAddress()); 
    assignedAuthor.appendChild(addr); 

    Element[] telecom = generateTelecom(authorOrganizationIdentity.getTelecomList()); 
    for (int i = 0; i < telecom.length; i++) {
      assignedAuthor.appendChild(telecom[i]); 
    }
 
    Element assignedPerson = getDocument().createElement("assignedPerson");
    assignedPerson.setAttribute("classCode", "PSN");
    assignedPerson.setAttribute("determinerCode", "INSTANCE"); 
    Element name = generateName(authorIdentity); 
    assignedPerson.appendChild(name); 
    assignedAuthor.appendChild(assignedPerson); 

    Element representedOrganization = getDocument().createElement("representedOrganization");
    representedOrganization.setAttribute("classCode", "ORG");
    representedOrganization.setAttribute("determinerCode", "INSTANCE");
    assignedAuthor.appendChild(representedOrganization);

    Element nameOfOrg = getDocument().createElement("name"); 
    nameOfOrg.setTextContent(authorOrganizationIdentity.getOrgName());
    representedOrganization.appendChild(nameOfOrg);
  } 

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
    Element custodian = getDocument().createElement("custodian");
    custodian.setAttribute("typeCode", "CST");
    root.appendChild(custodian);

    Element assignedCustodian = getDocument().createElement("assignedCustodian");
    assignedCustodian.setAttribute("classCode", "ASSIGNED");
    custodian.appendChild(assignedCustodian); 

    Element representedCustodianOrganization = getDocument().createElement("representedCustodianOrganization");
    representedCustodianOrganization.setAttribute("classCode", "ORG");
    representedCustodianOrganization.setAttribute("determinerCode", "INSTANCE"); 
    assignedCustodian.appendChild(representedCustodianOrganization);

    Element id = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, custodianIdentity.getOrgOID());
    representedCustodianOrganization.appendChild(id); 

    Element name = getDocument().createElement("name"); 
    name.setTextContent(custodianIdentity.getOrgName());
    representedCustodianOrganization.appendChild(name); 

    Element[] telecom = generateTelecom(custodianIdentity.getTelecomList()); 
    for (int i = 0; i < telecom.length; i++) {
      representedCustodianOrganization.appendChild(telecom[i]); 
    }

    Element addr = generateAddr(custodianIdentity.getAddress()); 
    representedCustodianOrganization.appendChild(addr); 
 
    assignedCustodian.appendChild(representedCustodianOrganization); 
  }

  @Override
  public void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity,
      Date timeOfAutentication) {
    // Section 2.13.4

    Element legalAuthenticator = getDocument().createElement("legalAuthenticator");
    legalAuthenticator.setAttribute("typeCode", "LA");
    legalAuthenticator.setAttribute("contextControlCode", "OP");
    root.appendChild(legalAuthenticator);

    Element timeElement = getDocument().createElement("time");
    timeElement.setAttribute("value", dateTimeformatter.format(timeOfAutentication));
    legalAuthenticator.appendChild(timeElement);

    Element signatureCode = getDocument().createElement("signatureCode");
    signatureCode.setAttribute("nullFlavor", "NI");
    legalAuthenticator.appendChild(signatureCode);

    // Assigned entity
    Element assignedEntity = getDocument().createElement("assignedEntity");
    assignedEntity.setAttribute("classCode", "ASSIGNED");
    legalAuthenticator.appendChild(assignedEntity);

    Element id = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, authenticatorOrganizationIdentity.getOrgOID());
    assignedEntity.appendChild(id);

    Element addr = generateAddr(authenticatorOrganizationIdentity.getAddress()); 
    assignedEntity.appendChild(addr); 

    Element[] telecom = generateTelecom(authenticatorOrganizationIdentity.getTelecomList()); 
    for (int i = 0; i < telecom.length; i++) {
      assignedEntity.appendChild(telecom[i]); 
    }

    Element assignedPerson = getDocument().createElement("assignedPerson");
    assignedPerson.setAttribute("classCode", "PSN");
    assignedPerson.setAttribute("determinerCode", "INSTANCE");
    assignedEntity.appendChild(assignedPerson);

    Element name = generateName(authenticatorPersonIdentity); 
    assignedPerson.appendChild(name); 

    Element representedOrganization = getDocument().createElement("representedOrganization");
    representedOrganization.setAttribute("classCode", "ORG");
    representedOrganization.setAttribute("determinerCode", "INSTANCE");
    assignedEntity.appendChild(representedOrganization);

    Element nameOfOrg = getDocument().createElement("name"); 
    nameOfOrg.setTextContent(authenticatorOrganizationIdentity.getOrgName());
    representedOrganization.appendChild(nameOfOrg);
  }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
    Element documentationOf = getDocument().createElement("documentationOf");
    documentationOf.setAttribute("typeCode", "DOC");
    root.appendChild(documentationOf);

    Element serviceEvent = getDocument().createElement("serviceEvent");
    serviceEvent.setAttribute("classCode", "MPROT");
    serviceEvent.setAttribute("moodCode", "EVN");
    documentationOf.appendChild(serviceEvent);

    Element effectiveTime = getDocument().createElement("effectiveTime");
    serviceEvent.appendChild(effectiveTime);

    Element low = getDocument().createElement("low");
    low.setAttribute("value", dateTimeformatter.format(serviceStart));
    effectiveTime.appendChild(low);

    Element high = getDocument().createElement("high");
    high.setAttribute("value", dateTimeformatter.format(serviceEnd));
    effectiveTime.appendChild(high); 
  }

  @Override
  public void buildStructuredBodySection() {
    // =================================================== Body 
    // Section 3.1 
    // The CDA book p 171 
    Element component = getDocument().createElement("component");
    component.setAttribute("typeCode", "COMP"); 
    component.setAttribute("contextConductionInd", "true");

    root.appendChild(component); 

    // CONF-PHMR-43 
    structuredBody = getDocument().createElement("structuredBody");
    structuredBody.setAttribute("classCode", "DOCBODY"); 
    structuredBody.setAttribute("moodCode", "EVN"); 
    component.appendChild(structuredBody); 
  }

  private Element sectionContainingObservations;

  @Override
  public void buildResults(List<Measurement> results) {

    // CONF-DK-PHMR-27
    sectionContainingObservations = 
      buildSubComponentIn("2.16.840.1.113883.10.20.1.14", MedCom.DK_PHMR_ROOT_POD, "30954-2", "Results");

    if (results.size() == 0) {
      Element text = getDocument().createElement("text");
      text.setTextContent("No Results");
      sectionContainingObservations.appendChild(text); 
    } else {
      for (Measurement m : results) {
        buildMeasurement(m);
      }
    }
  }

  @Override
  public void buildVitalSigns(List<Measurement> vitalSigns) {

    // CONF-DK-PHMR-26
    sectionContainingObservations = 
      buildSubComponentIn("2.16.840.1.113883.10.20.1.16", MedCom.DK_PHMR_ROOT_POD, "8716-3", "Vital Signs");

    if (vitalSigns.size() == 0) {
      Element text = getDocument().createElement("text");
      text.setTextContent("No Vital Signs");
      sectionContainingObservations.appendChild(text); 
    } else {
      for (Measurement m : vitalSigns) {
        buildMeasurement(m);
      }
    }
  }

  private void buildMeasurement(Measurement measurement) {
    // CONF-PHMR-58 
    Element entry = getDocument().createElement("entry");
    entry.setAttribute("typeCode", "COMP");
    entry.setAttribute("contextConductionInd", "true");
    sectionContainingObservations.appendChild(entry); 

    Element organizer = getDocument().createElement("organizer");
    organizer.setAttribute("classCode", "CLUSTER"); 
    organizer.setAttribute("moodCode", "EVN"); 
    entry.appendChild(organizer); 

    // PHMR CONF-381
    appendTemplateID(organizer, HL7.OBSERVATION_ORGANIZER); 

    Element statusCode = getDocument().createElement("statusCode"); 
    statusCode.setAttribute("code", (measurement.getCompleted()) ? "completed" : "nullified"); 
    organizer.appendChild(statusCode); 

    appendEffectiveTime(organizer, measurement.getTimestamp());

    // Component 
    Element component = getDocument().createElement("component");
    component.setAttribute("typeCode", "COMP");
    component.setAttribute("contextConductionInd", "true");
    organizer.appendChild(component); 

    Element observationElement = getDocument().createElement("observation"); 
    observationElement.setAttribute("classCode", "OBS"); 
    observationElement.setAttribute("moodCode", "EVN"); 
    component.appendChild(observationElement); 

    String 
      codeSystem = measurement.getCodeSystem(),
      codeSystemName = measurement.getCodeSystemName(); 

    // PHMR CONF-381
    appendTemplateID(observationElement, HL7.OBSERVATION);
    if (measurement instanceof NumericMeasurement) {
      NumericMeasurement numeric = (NumericMeasurement) measurement;
      String 
        value = numeric.getValue(),
        unit = numeric.getUnit(), 
        code = numeric.getCode(), 
        displayName = numeric.getDisplayName();

      appendTemplateID(observationElement, HL7.PHMR_NUMERIC_OBSERVATION);
      appendCode(observationElement, code, codeSystem, displayName, codeSystemName);

      Element valueElement = getDocument().createElement("value"); 
      observationElement.appendChild(valueElement); 
      valueElement.setAttribute("xsi:type", "PQ");  // Maybe introduce a namespace for xsi 
      valueElement.setAttribute("value", value); 
      valueElement.setAttribute("unit", unit); 
    } else {
      Element codeElement = getDocument().createElement("code"); 
      codeElement.setAttribute("nullFlavor", "NI");
      observationElement.appendChild(codeElement); 
    }

    if (measurement instanceof UrlToMediaMeasurement) {
      UrlToMediaMeasurement urlToMedia = (UrlToMediaMeasurement) measurement;

      // TODO: validate this section.
      Element entryRelationship = getDocument().createElement("entryRelationship"); 
      entryRelationship.setAttribute("contextConductionInd", "true"); 
      entryRelationship.setAttribute("typeCode", "COMP"); 
      observationElement.appendChild(entryRelationship);

      Element observationMedia = getDocument().createElement("observationMedia"); 
      observationMedia.setAttribute("ID", "UrlToMedia"); 
      observationMedia.setAttribute("classCode", "OBS"); 
      observationMedia.setAttribute("moodCode", "EVN"); 
      entryRelationship.appendChild(observationMedia);

      // TODO: validate OID and parameterize extension.
      Element id = createId(MedCom.ROOT_OID, MedCom.ROOT_AUTHORITYNAME, urlToMedia.getId());
      observationMedia.appendChild(id);

      Element valueElm = getDocument().createElement("value"); 
      observationMedia.appendChild(valueElm);

      Element reference = getDocument().createElement("reference");
      reference.setAttribute("value", urlToMedia.getReference()); 
      valueElm.appendChild(reference);
    }

    // In case of context information, make a methodCode
    buildContext(observationElement, measurement.getContext());

    // entryRelationship 
    if (measurement.hasComment()) {
      org.net4care.phmr.model.Comment comment = measurement.getComment();

      Element entryRelationship = getDocument().createElement("entryRelationship"); 
      entryRelationship.setAttribute("contextConductionInd", "true"); 
      entryRelationship.setAttribute("typeCode", "COMP"); 
      observationElement.appendChild(entryRelationship);

      Element act = getDocument().createElement("act"); 
      act.setAttribute("classCode", "ACT"); 
      act.setAttribute("moodCode", "EVN"); 
      entryRelationship.appendChild(act);

      appendLoincCode(act, Loinc.COMMENT, Loinc.COMMENT_DISPLAYNAME);

      Element text = getDocument().createElement("text");
      text.setTextContent(comment.getText());
      act.appendChild(text);

      Element author = getDocument().createElement("author");
      author.setAttribute("contextControlCode", "OP"); 
      author.setAttribute("typeCode", "AUT"); 
      act.appendChild(author);

      Element time = getDocument().createElement("time");
      time.setAttribute("value", dateTimeformatter.format(comment.getTime())); 
      author.appendChild(time);

      Element assignedAuthor = getDocument().createElement("assignedAuthor");
      assignedAuthor.setAttribute("classCode", "ASSIGNED");
      author.appendChild(assignedAuthor);

      String organizationId = comment.getOrganization().getOrgOID();
      Element id = createId(NSI.SOR_OID, NSI.SOR_AUTHORITYNAME, organizationId); 
      assignedAuthor.appendChild(id);

      Element assignedPerson = getDocument().createElement("assignedPerson");
      assignedPerson.setAttribute("classCode", "PSN");
      assignedPerson.setAttribute("determinerCode", "INSTANCE");
      assignedAuthor.appendChild(assignedPerson);

      Element name = generateName(comment.getAuthor());
      assignedPerson.appendChild(name);
    }
  }

  private void buildContext(Element observationElement, Context context) {
    if (context != null) {
      Element methodCode1Element = getDocument().createElement("methodCode");

      String performerCode, performerDisplayName, participantRoleCode;
      switch (context.getMeasurementPerformerCode()) {
      case Citizen:
        performerCode = MedCom.PERFORMED_BY_CITIZEN;
        performerDisplayName = MedCom.PERFORMED_BY_CITIZEN_DISPLAYNAME;
        participantRoleCode = "CIT"; 
        break;
      case HealthcareProfessional:
        performerCode = MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL;
        performerDisplayName = MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
        participantRoleCode = "ASSIGNED";
        break;
      case CareGiver:
        performerCode = MedCom.PERFORMED_BY_CAREGIVER;
        performerDisplayName = MedCom.PERFORMED_BY_CAREGIVER_DISPLAYNAME;
        participantRoleCode = "CAREGIVER";
        break;
      default:
        throw new IllegalStateException("Invalid Context performer code" + context.getMeasurementPerformerCode().toString());
      }

      String provisionCode, provisionDisplayName;
      switch (context.getMeasurementProvisionMethodCode()) {
      case Electronicallly:
        provisionCode = MedCom.TRANSFERRED_ELECTRONICALLY;
        provisionDisplayName = MedCom.TRANSFERRED_ELECTRONICALLY_DISPLAYNAME;
        break;
      case TypedByCitizen:
        provisionCode = MedCom.TYPED_BY_CITIZEN;
        provisionDisplayName = MedCom.TYPED_BY_CITIZEN_DISPLAYNAME;
        break;
      case TypedByCitizenRelative:
        provisionCode = MedCom.TYPED_BY_CITIZEN_RELATIVE;
        provisionDisplayName = MedCom.TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME;
        break;
      case TypedByHealthcareProfessional:
        provisionCode = MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL;
        provisionDisplayName = MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
        break;
      case TypedByCareGiver:
        provisionCode = MedCom.TYPED_BY_CAREGIVER;
        provisionDisplayName = MedCom.TYPED_BY_CAREGIVER_DISPLAYNAME;
        break;
      default:
        throw new IllegalStateException("Invalid Context provision method code" + context.getMeasurementProvisionMethodCode().toString());
      }

      methodCode1Element.setAttribute("code", performerCode);
      methodCode1Element.setAttribute("codeSystem", MedCom.MESSAGECODE_OID);
      methodCode1Element.setAttribute("displayName", performerDisplayName);
      methodCode1Element.setAttribute("codeSystemName", MedCom.MESSAGECODE_DISPLAYNAME);
      observationElement.appendChild(methodCode1Element);
 
      Element methodCode2Element = getDocument().createElement("methodCode");
      methodCode2Element.setAttribute("code", provisionCode);
      methodCode2Element.setAttribute("codeSystem", MedCom.MESSAGECODE_OID);
      methodCode2Element.setAttribute("displayName", provisionDisplayName);
      methodCode2Element.setAttribute("codeSystemName", MedCom.MESSAGECODE_DISPLAYNAME);
      observationElement.appendChild(methodCode2Element);

      /* TODO: validate
      OrganizationIdentity organization = context.getOrganizationIdentity();
      if (organization != null) {
        Element participant = getDocument().createElement("participant"); 
        participant.setAttribute("contextControlCode", "OP"); 
        participant.setAttribute("typeCode", "ENT"); 
        observationElement.appendChild(participant);

        Element participantRole = getDocument().createElement("participantRole"); 
        participantRole.setAttribute("classCode", patientRoleCode); 
        participant.appendChild(participantRole);

        Element id = getDocument().createElement("id"); 
        id.setAttribute("extension", organization.getOrgOID()); 
        id.setAttribute("root", NSI.SOR_OID); 
        participantRole.appendChild(id);
      } */
    }
  }

  private Element sectionContainingMedicalEquipment;

  @Override
  public void buildMedicalEquipmentSection(List<MedicalEquipment> equipments) {

    // CONF-DK-PHMR- 24
    sectionContainingMedicalEquipment = 
        buildSubComponentIn("2.16.840.1.113883.10.20.1.7", 
          "2.16.840.1.113883.3.4208.11.1", "46264-8", "Medical Equipment"); 

    for (MedicalEquipment equipment : equipments) {
      // CONF-PHMR-50 - Section 3.5.2: Device Definition Organizers 
      Element entry = getDocument().createElement("entry");
      entry.setAttribute("contextConductionInd", "true");
      entry.setAttribute("typeCode", "COMP"); 

      Element organizer = getDocument().createElement("organizer"); 
      // CONF-PHMR-69 
      organizer.setAttribute("classCode", "CLUSTER"); 
      organizer.setAttribute("moodCode", "EVN"); 
      // CONF-PHMR-70 
      appendTemplateID(organizer, HL7.DEVICE_DEFINITION_ORGANIZER); 

      Element status2 = getDocument().createElement("statusCode"); 
      status2.setAttribute("code", "completed"); 
      organizer.appendChild(status2); 

      // CONF-PHMR-71 / Boone p. 209 
      Element participant = getDocument().createElement("participant"); 
      organizer.appendChild(participant); 

      participant.setAttribute("typeCode", "SBJ"); 
      participant.setAttribute("contextControlCode", "OP");
      appendTemplateID(participant, HL7.PHMR_PRODUCT_INSTANCE);

      Element participantrole = getDocument().createElement("participantRole"); 
      participantrole.setAttribute("classCode", "MANU"); 
      participant.appendChild(participantrole); 

      Element playdevice = getDocument().createElement("playingDevice");
      playdevice.setAttribute("classCode", "DEV");
      playdevice.setAttribute("determinerCode", "INSTANCE");
      participantrole.appendChild(playdevice); 

      // Insert device code
      Element code = getDocument().createElement("code");
      code.setAttribute("code", equipment.getMedicalDeviceCode());
      code.setAttribute("codeSystem", MedCom.DEVICE_OID); 
      code.setAttribute("displayName", equipment.getMedicalDeviceDisplayName());
      playdevice.appendChild(code);

      // Insert manufacturer model & software
      Element manufacturerModel = getDocument().createElement("manufacturerModelName");
      manufacturerModel.setTextContent(equipment.getManufacturerModelName());
      playdevice.appendChild(manufacturerModel);

      Element softwareNameElement = getDocument().createElement("softwareName");
      softwareNameElement.setTextContent(equipment.getSoftwareName());
      playdevice.appendChild(softwareNameElement);

      // Figure 24 indicate participant role object, x-check CCD 
      entry.appendChild(organizer); 

      sectionContainingMedicalEquipment.appendChild(entry); 
    }
  }

  // === Helper methods ===

  private Element generateName(PersonIdentity patientIdentity) {
    Element name = getDocument().createElement("name"); 
    if (patientIdentity.hasPrefix()) {
      Element prefix = getDocument().createElement("prefix"); 
      prefix.setTextContent(patientIdentity.getPrefix()); 
      name.appendChild(prefix); 
    }
    for (int i = 0; i < patientIdentity.getGivenNames().length; i++) {
      Element given = getDocument().createElement("given"); 
      given.setTextContent(patientIdentity.getGivenNames()[i]);
      name.appendChild(given); 
    }
    Element family = getDocument().createElement("family"); 
    family.setTextContent(patientIdentity.getFamilyName()); 
    name.appendChild(family); 
    return name; 
  }

  private Element[] generateTelecom(Telecom[] theList) {
    Element[] returnList = new Element[theList.length]; 
    for (int i = 0; i < theList.length; i++) {
      Element telecom = getDocument().createElement("telecom"); 
      telecom.setAttribute("value", theList[i].getValue());
      telecom.setAttribute("use", translateToHL7String(theList[i].getAddressUse()));
      returnList[i] = telecom;
    }
    return returnList; 
  }

  private String translateToHL7String(AddressData.Use use) {
    String useString = "ERROR";
    if (use == AddressData.Use.HomeAddress) {
      useString = "H";
    }
    if (use == AddressData.Use.WorkPlace) {
      useString = "WP";
    }
    return useString;
  }

  private Element generateAddr(AddressData addrData) {
    Element addr = getDocument().createElement("addr");
    addr.setAttribute("use", translateToHL7String(addrData.getAddressUse()));

    String[] streeeLines = addrData.getStreet();
    for (int i = 0; i < streeeLines.length; i++) {
      Element streetAddressLine = getDocument().createElement("streetAddressLine");
      streetAddressLine.setTextContent(streeeLines[i]);
      addr.appendChild(streetAddressLine); 
    }
    Element postalCode = getDocument().createElement("postalCode"); 
    postalCode.setTextContent(addrData.getPostalCode()); 
    addr.appendChild(postalCode); 

    Element city = getDocument().createElement("city"); 
    city.setTextContent(addrData.getCity()); 
    addr.appendChild(city); 

    Element country = getDocument().createElement("country"); 
    country.setTextContent(addrData.getCountry()); 
    addr.appendChild(country); 

    return addr; 
  }

  private Element appendTemplateID(Element section, String rootOID) { 
    Element templateId; 
    templateId = getDocument().createElement("templateId"); 
    templateId.setAttribute("root", rootOID); 
    section.appendChild(templateId); 
    return templateId; 
  } 

  private Element appendCode(Element context, String code, String codeSystem, 
      String displayName, String codeSystemName) { 
    Element codeElement = appendCode(context, code, codeSystem); 
    codeElement.setAttribute("displayName", displayName);
    codeElement.setAttribute("codeSystemName", codeSystemName);
    return codeElement; 
  } 

  private Element appendCode(Element context, String code, String codeSystem) { 
    Element codeElement = getDocument().createElement("code"); 
    codeElement.setAttribute("code", code);
    codeElement.setAttribute("codeSystem", codeSystem);
    context.appendChild(codeElement); 
    return codeElement; 
  }

  private Element appendLoincCode(Element context, String code, String displayName) { 
    Element codeElement = appendCode(context, code, Loinc.OID); 
    codeElement.setAttribute("displayName" , displayName);
    codeElement.setAttribute("codeSystemName", Loinc.DISPLAYNAME);
    return codeElement; 
  } 

  private Element buildSubComponentIn(String templateOID1, String templateOID2, 
      String sectionCodeInLOINC, String textLabel) {
    // The CDA book p 174 - fig 15.5 
    Element sectionComponent = getDocument().createElement("component");
    sectionComponent.setAttribute("typeCode", "COMP");
    sectionComponent.setAttribute("contextConductionInd", "true");
    structuredBody.appendChild(sectionComponent);

    // Sec 3.3.3 Results 30954-2 
    sectionContainingObservations = getDocument().createElement("section");
    sectionContainingObservations.setAttribute("classCode", "DOCSECT");
    sectionContainingObservations.setAttribute("moodCode", "EVN");
    sectionComponent.appendChild(sectionContainingObservations); 
 
    appendTemplateID(sectionContainingObservations, templateOID1); 
    appendTemplateID(sectionContainingObservations, templateOID2); 
    appendCode(sectionContainingObservations, sectionCodeInLOINC, Loinc.OID); // Relevant diagnostic tests or laboratory data 

    // Unstructured Part 
    Element title = getDocument().createElement("title"); 
    title.setTextContent(textLabel); 
    sectionContainingObservations.appendChild(title); 

    return sectionContainingObservations;
  }



}
