package org.net4care.phmr.builders;

import java.util.Date;
import java.util.List;

import org.net4care.phmr.model.*;

/** A builder that just constructs a plain string representation
 * of a SimpleClinicalDocument. Used by the toString() method
 * of the CDA.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class PlainStringBuilder implements PhmrBuilder {

  private String result;

  /** Get the plain string result.
   * 
   * @return The plain string result.
   */
  public String getResult() {
    return result;
  }

  @Override
  public void buildRootNode() {
    result = "SimpleClinicalDocument:\n"; 
  }

  @Override
  public void buildHeader() {
  }

  @Override
  public void buildContext(PersonIdentity patientIdentity, Date effectiveTime,
      String setId, String versionNumber) {
    result += " setId: " + setId + " / version: " + versionNumber + "\n";
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {
    result += " Patient: " + patientIdentity.getSSN() + ": " + patientIdentity.getGivenNames()[0] + " " + patientIdentity.getFamilyName() + "\n";
  }

  @Override
  public void buildResults(List<Measurement> results) {
    result += " Results:\n";

    if (results.size() == 0) {
      result += "    No Results";
    } else {
      for (Measurement m : results) {
        buildMeasurement(m);
      }
    }
  }

  @Override
  public void buildVitalSigns(List<Measurement> vitalSigns) {
    result += " Vital Signs:\n";

    if (vitalSigns.size() == 0) {
      result += "    No Vital Signs";
    } else {
      for (Measurement m : vitalSigns) {
        buildMeasurement(m);
      }
    }
  }

  private int count = 0;
  private void buildMeasurement(Measurement m) {
    result += "    " + count + ": " + m.getTimestamp() + " :: ";
    if (m instanceof NumericMeasurement) {
      NumericMeasurement n = (NumericMeasurement) m;
      result += n.getDisplayName() + " " + n.getValue() + " " + n.getUnit();
    } else if (m instanceof UrlToMediaMeasurement) {
      UrlToMediaMeasurement u = (UrlToMediaMeasurement) m;
      result += u.getReference();
    }
    result += "\n";
    count++;
  }

  @Override
  public void buildStructuredBodySection() {

  }

  @Override
  public void buildAuthorSection(
      OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStartTime) {
    result += " Author: \n";
    result += "   " + authorOrganizationIdentity.getOrgName() + "\n";
    result += "    - " + authorIdentity.getGivenNames()[0] + " " + authorIdentity.getFamilyName() + "\n";
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
    result += " Custodian: \n";
    result += "   " + custodianIdentity.getOrgName() + "\n";

  }

  @Override
  public void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    result += " Authenticator: \n";
    result += "   " + authenticatorOrganizationIdentity.getOrgName() + "\n";
    result += "    - " + authenticatorPersonIdentity.getGivenNames()[0] + " " + authenticatorPersonIdentity.getFamilyName() + "\n";
  }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
    // TODO Auto-generated method stub

  }

  @Override
  public void buildMedicalEquipmentSection(List<MedicalEquipment> equipments) {
    result += " Medical device:\n";
    for (MedicalEquipment equipment : equipments) {
      result += equipment.getManufacturerModelName() + " " + equipment.getSoftwareName() + "\n";
    }
  }
}
