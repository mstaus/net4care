package org.net4care.phmr.builders;

/** Strategy to generate UUID. 
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public interface UUIDStrategy {

  /** Generate the UUID string.
   * 
   *  @return Generated UUID string.
   */
  String generate();
}
