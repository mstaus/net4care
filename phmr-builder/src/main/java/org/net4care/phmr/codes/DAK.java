package org.net4care.phmr.codes;

import java.util.Date;

import org.net4care.phmr.model.Context;
import org.net4care.phmr.model.Measurement;
import org.net4care.phmr.model.NumericMeasurement;

//CHECKSTYLE:OFF
public class DAK {

  public static final String DISPLAYNAME     = "DAK-e datafangst";
  public static final String CODESYSTEM_OID  = "2.16.840.1.113883.3.4208.100.8";

  public static final String FVC = "MCS88016";

  public static Measurement createFVC(String fvc, Date atTime, Context context) {
    return new NumericMeasurement(DAK.FVC, "FVC", fvc, "L", atTime, true, context, null);
  }
}
