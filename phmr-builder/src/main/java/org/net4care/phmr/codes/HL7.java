package org.net4care.phmr.codes;

//CHECKSTYLE:OFF
public class HL7 {

  // International codes
  public static final String PHMR_DK_TYPEID_EXTENSION   = "POCD_HD000040";
  public static final String PHMR_TYPEID_ROOT           = "2.16.840.1.113883.1.3";

  public static final String CONFIDENTIALITY_OID        = "2.16.840.1.113883.5.25";
  public static final String GENDER_OID                 = "2.16.840.1.113883.5.1";

  public static final String OBSERVATION_ORGANIZER      = "2.16.840.1.113883.10.20.1.35";
  public static final String OBSERVATION                = "2.16.840.1.113883.10.20.1.31";
  public static final String DEVICE_DEFINITION_ORGANIZER = "2.16.840.1.113883.10.20.9.4";
  public static final String PHMR_NUMERIC_OBSERVATION   = "2.16.840.1.113883.10.20.9.8";
  public static final String PHMR_PRODUCT_INSTANCE      = "2.16.840.1.113883.10.20.9.9";
}
