package org.net4care.phmr.codes;

//CHECKSTYLE:OFF
public class Loinc {
  public static final String OID = "2.16.840.1.113883.6.1";
  public static final String DISPLAYNAME = "LOINC";

  public static final String PHMR_CODE = "53576-5";
  public static final String PMHR_DISPLAYNAME = "Personal Health Monitoring Report";

  public static final String COMMENT = "48767-8";
  public static final String COMMENT_DISPLAYNAME = "Kommentar til måling";
}
