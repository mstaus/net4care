package org.net4care.phmr.codes;

// CHECKSTYLE:OFF
public class MedCom {

  public static final String ROOT_AUTHORITYNAME  = "MedCom";
  public static final String ROOT_OID            = "2.16.840.1.113883.3.4208";

  public static final String CPR_AUTHORITYNAME   = "CPR";
  public static final String CPR_OID             = "2.16.840.1.113883.3.4208.100.2";

  public static final String MESSAGECODE_DISPLAYNAME = "MedCom Message Codes"; 
  public static final String MESSAGECODE_OID     = "2.16.840.1.113883.3.4208.100.6"; 
  public static final String DEVICE_OID          = "2.16.840.1.113883.3.4208.100.7";

  public static final String DK_PHMR_ROOT_POD    = "2.16.840.1.113883.3.4208.11.1";

  // MedCom HL7 to identify the performer of a measurement
  public static final String PERFORMED_BY_CITIZEN = "POT";
  public static final String PERFORMED_BY_HEALTHCAREPROFESSIONAL = "PNT";
  public static final String PERFORMED_BY_CAREGIVER = "PCG";

  // Related displaynames
  public static final String PERFORMED_BY_CITIZEN_DISPLAYNAME = "Målt af borger";
  public static final String PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME = "Målt af aut. sundhedsperson";
  public static final String PERFORMED_BY_CAREGIVER_DISPLAYNAME= "Målt af anden omsorgsperson";

  // MedCom codes to identify how data is provided to the data collection device
  public static final String TRANSFERRED_ELECTRONICALLY = "AUT";
  public static final String TYPED_BY_CITIZEN = "TPD";
  public static final String TYPED_BY_CITIZEN_RELATIVE = "TPR";
  public static final String TYPED_BY_HEALTHCAREPROFESSIONAL = "TPH";
  public static final String TYPED_BY_CAREGIVER = "TPC";

  // Related displaynames
  public static final String TRANSFERRED_ELECTRONICALLY_DISPLAYNAME = "Måling overført automatisk";
  public static final String TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME = "Indtastet af pårørende";
  public static final String TYPED_BY_CITIZEN_DISPLAYNAME = "Indtastet af borger";
  public static final String TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME = "Indtastet af aut. sundhedsperson";
  public static final String TYPED_BY_CAREGIVER_DISPLAYNAME = "Indtastet af anden omsorgsperson";
}
