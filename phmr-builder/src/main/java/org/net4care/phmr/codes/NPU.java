package org.net4care.phmr.codes;

import java.util.Date;

import org.net4care.phmr.model.Context;
import org.net4care.phmr.model.Measurement;
import org.net4care.phmr.model.NumericMeasurement;
import org.net4care.phmr.model.UrlToMediaMeasurement;

//CHECKSTYLE:OFF
public class NPU {
  // Often used codes in NPU for telemedical measurements

  public static final String DISPLAYNAME     = "NPU terminologien";
  public static final String CODESYSTEM_OID  = "2.16.840.1.113883.3.4208.100.1";

  public static final String DRY_BODY_WEIGHT_CODE = "NPU03804";
  public static final String BLOOD_PRESURE_SYSTOLIC_ARM = "DNK05472";
  public static final String BLOOD_PRESURE_DIASTOLIC_ARM = "DNK05473";
  public static final String PROTEIN_URINE = "NPU03958";
  public static final String SATURATION = "NPU03011";

  public static Measurement createWeight(String weightInKg, Date atTime, Context context) {
    return new NumericMeasurement(NPU.DRY_BODY_WEIGHT_CODE, "Legeme masse; Pt", weightInKg, "kg", atTime, true, context, null);
  }

  public static Measurement createBloodPresureSystolic(String systolic, Date atTime, Context context) {
    // TODO: this should be mm[Hg]
    return new NumericMeasurement(NPU.BLOOD_PRESURE_SYSTOLIC_ARM, "Blodtryk systolisk; Arm", systolic, "mmHg", atTime, true, context, null);
  }

  public static Measurement createBloodPresureDiastolic(String diastolic, Date atTime, Context context) {
    // TODO: this should be mm[Hg]
    return new NumericMeasurement(NPU.BLOOD_PRESURE_DIASTOLIC_ARM, "Blodtryk diastolisk; Arm", diastolic, "mmHg", atTime, true, context, null);
  }

  public static Measurement createProteinUrine(String protein, Date atTime, Context context) {
    return new NumericMeasurement(NPU.PROTEIN_URINE, "Protein;U", protein, "g/L", atTime, true, context, null);
  }

  public static Measurement createSaturation(String saturation, Date atTime, Context context) {
    return new NumericMeasurement(NPU.SATURATION, "O2 sat.;Hb(aB)", saturation, "%", atTime, true, context, null);
  }
 
  public static Measurement createCTG(String id, String reference, Date atTime, Context context) {
    return new UrlToMediaMeasurement(id, reference, atTime, true, context, null);
  }
}
