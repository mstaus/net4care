package org.net4care.phmr.model;

/** Data structure to contain address data.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class AddressData { 

  /** Address data use enumeration.
   */
  public enum Use {
    /** Home address. */
    HomeAddress,
    /** Work place address. */
    WorkPlace,
  }

  private String[] streetLines; 
  private String postalCode; 
  private String city; 
  private String country; 
  private Use addressUse; 

  /** AddressData constructor.
   * 
   * @param use The use of the address, home or work place.
   * @param streetLines A list of the street address lines.
   * @param postalCode The postal code.
   * @param city The city name.
   * @param country The country name.
   */
  public AddressData(Use use, String[] streetLines, String postalCode, String city, String country) { 
    super(); 
    addressUse = use;
    this.streetLines = streetLines; 
    this.postalCode = postalCode; 
    this.city = city; 
    this.country = country;
  } 

  /** Get the list of streets. 
   * @return list of streets.
   */
  public String[] getStreet() { 
    return streetLines; 
  }

  /** Get the postal code.
  * @return postal code.
  */
  public String getPostalCode() { 
    return postalCode; 
  }

  /** Get the city name.
  * @return city name.
  */
  public String getCity() { 
    return city; 
  }

  /** Get the country name.
  * @return country name.
  */
  public String getCountry() { 
    return country; 
  }

  /** Get the address use.
  * @return address use.
  */
  public Use getAddressUse() {
    return addressUse;
  } 
} 
