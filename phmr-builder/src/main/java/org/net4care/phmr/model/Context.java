package org.net4care.phmr.model;

/** The Context class encapsulate the methodCodes that
 * may provide further context information regarding a
 * measurement.
 * 
 * See section 6.2 regarding the MedCom codes in the
 * Danish PHMR profile for the actual codes.
 * 
 * Usage: You create a measurement and use the
 * extended constructor that allows you to supply
 * a context object in addition to the actual
 * measurements. The context object must be
 * supplied with the codes to use for who
 * performed the measurement and how data was
 * provided. You should use the constant strings
 * defined within this class.
 * 
 * Example: a measurement made by the citizen and
 * next typed in by the citizen can be defined by
 * this context:
 * 
 * Context c = new Context(
 *   MEASUREMENT_PERFORMED_BY_CITIZEN,
 *   MEASUREMENT_TYPED_BY_CITIZEN);
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */

public final class Context {

  /** Performer type enumeration.
   */
  public enum PerformerType {
    /** Citizen performer type. */
    Citizen,
    /** Healthcare professional performer type. */
    HealthcareProfessional,
    /** Care giver performer type. */
    CareGiver
  }

  /** Provision method. */
  public enum ProvisionMethod {
    /** Provisioned electronically. */
    Electronicallly,
    /** Typed by citizen. */
    TypedByCitizen,
    /** Typed by citizen relative. */
    TypedByCitizenRelative,
    /** Typed by healthcare professional. */
    TypedByHealthcareProfessional,
    /** Typed by care giver. */
    TypedByCareGiver
  }

  private ProvisionMethod dataProvision;
  private PerformerType measurementActor;
  private OrganizationIdentity organizationIdentity;

  /** Construct a measurement with the given performer and given
   * provision.
   * @param measurementProvisionCode a MedCom code that identifies
   * how the measured values are provided to the data collection
   * device (typically, electronic transferral or typed in)
   * @param measurementPerformerCode a MedCom code that identifies the
   * provider of the measurement (the citizen himself, a relative, etc.)
   * @param organizationIdentity The organization identity.
   */
  public Context(ProvisionMethod measurementProvisionCode, PerformerType measurementPerformerCode, OrganizationIdentity organizationIdentity) {
    this.measurementActor = measurementPerformerCode;
    this.dataProvision = measurementProvisionCode;
    this.organizationIdentity = organizationIdentity;
  }

  /** Get the code of the measurement performer.
   * @return the code of the measurement performer
   */
  public PerformerType getMeasurementPerformerCode() {
    return measurementActor;
  }

  /** Get the code of the measurement provision method.
   * @return the code of the measurement provision method
   */
  public ProvisionMethod getMeasurementProvisionMethodCode() {
    return dataProvision;
  }

  /** Get organization identity.
   * @return organization identity.
   */
  public OrganizationIdentity getOrganizationIdentity() {
    return organizationIdentity;
  }
}
