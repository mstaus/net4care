package org.net4care.phmr.model;

import java.util.*;

/** An implementation of SimpleClinicalDocument that
 * only supports the data of the Danish PHMR.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */

public final class DanishPHMRModel implements SimpleClinicalDocument {

  private PersonIdentity patientIdentity = null;

  private PersonIdentity authorIdentity = null;
  private OrganizationIdentity authorOrganizationIdentity;

  private Date effectiveTime;
  private List<Measurement> resultList;
  private List<Measurement> vitalSignList;
  private List<MedicalEquipment> equipmentList;
  private Date authorParticipationStartTime;
  private OrganizationIdentity custodianIdentity;
  private OrganizationIdentity authenticatorOrganizationIdentity;
  private PersonIdentity authenticatorPersonIdentity;
  private Date timeOfAutentication;

  private Date serviceEnd;
  private Date serviceStart;

  private String setId;
  private String versionNumber;

  /** Danish PHMR model constructor.
   */
  public DanishPHMRModel() {
    resultList = new ArrayList<Measurement>();
    vitalSignList = new ArrayList<Measurement>();
    equipmentList = new ArrayList<MedicalEquipment>();
  }

  @Override
  public void setDocumentVersion(String uniqueId, int versionNumber) {
    this.setId = uniqueId;
   this.versionNumber = "" + versionNumber;
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setPatientRole(org.net4care.phmr.test.model.PersonIdentity)
   */
  @Override
  public void setPatient(PersonIdentity patientIdentity) {
    this.patientIdentity  = patientIdentity;
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#construct(org.net4care.phmr.test.Builder)
   */
  @Override
  public void construct(PhmrBuilder builder) {
    builder.buildRootNode();
    builder.buildHeader();
    builder.buildContext(patientIdentity, effectiveTime, setId, versionNumber);
    builder.buildPatientSection(patientIdentity);
    builder.buildAuthorSection(authorOrganizationIdentity, authorIdentity, authorParticipationStartTime);
    builder.buildCustodianSection(custodianIdentity);
    builder.buildLegalAuthenticatorSection(authenticatorOrganizationIdentity, 
        authenticatorPersonIdentity, timeOfAutentication);
    builder.buildDocumentationOf(serviceStart, serviceEnd);
    builder.buildStructuredBodySection();
    builder.buildVitalSigns(vitalSignList);
    builder.buildResults(resultList);
    builder.buildMedicalEquipmentSection(equipmentList);
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setDocumentationTime(java.util.Date, java.util.Date)
   */
  @Override
  public void setDocumentationTimeInterval(Date from, Date to) {
    serviceStart = from;
    serviceEnd = to;
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setEffectiveTime(java.util.Date)
   */
  @Override
  public void setEffectiveTime(Date now) {
    this.effectiveTime = now;
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setAuthor(org.net4care.phmr.test.model.PersonIdentity, java.util.Date)
   */
  @Override
  public void setAuthor(OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStart) {
    this.authorOrganizationIdentity = authorOrganizationIdentity;
    this.authorIdentity = authorIdentity;
    this.authorParticipationStartTime = authorParticipationStart;
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setCustodian(org.net4care.phmr.test.model.OrganizationIdentity)
   */
  @Override
  public void setCustodian(OrganizationIdentity custodianIdentity) {
    this.custodianIdentity = custodianIdentity;
  }

  /* (non-Javadoc)
   * @see org.net4care.phmr.test.ClinicalDocument#setAuthenticator(org.net4care.phmr.test.model.PersonIdentity)
   */
  @Override
  public void setAuthenticator(OrganizationIdentity authenticatorOrganizationIdentity,
        PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    this.authenticatorOrganizationIdentity = authenticatorOrganizationIdentity;
    this.authenticatorPersonIdentity = authenticatorPersonIdentity;
    this.timeOfAutentication = timeOfAuthentication;
  }

  @Override
  public void addResult(Measurement aMeasurement) {
    resultList.add(aMeasurement);
  }

  @Override
  public void addVitalSign(Measurement aMeasurement) {
    vitalSignList.add(aMeasurement);
  }

  @Override
  public void addMedicalEquipment(MedicalEquipment equipment) {
    equipmentList.add(equipment);
  }

  @Override
  public void addMedicalEquipment(String deviceCode, String deviceDisplayName, 
      String modelName, String softwareName) {
    MedicalEquipment equipment = new MedicalEquipment(deviceCode, deviceDisplayName, modelName, softwareName);
    equipmentList.add(equipment);
  }
}
