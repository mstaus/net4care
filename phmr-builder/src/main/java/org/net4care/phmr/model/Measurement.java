package org.net4care.phmr.model;

/** Basic measurement data type for holding measurements
 * in the SimpleClinicalDocument.
 * 
 * Note that the code system to use is defined statically,
 * so you have to invoke 'setCodeSystem' before making
 * any measurement instances. The set code system
 * will be effective for any subsequent measurements
 * made.
 * 
 * For Danish PHMR, you should use
 * 
 *     Measurement.setCodeSystem(HL7.NPU_OID, "NPU terminologien");
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
import java.util.Date;

/**
 * 
 */
public abstract class Measurement {

  private static String defaultCodeSystemOID = null;
  private static String defaultCodeSystemDisplayName = null;

  private String codeSystemOID;
  private String codeSystemDisplayName;

  private Date timestamp;
  private boolean completed;
  private Context context;
  private Comment comment;

  /** Define globally the code system to use for all subsequent
   * instanses of Measurement.
   * 
   * @param codeSystemOID The OID of the code system to use. 
   * @param diplayNameOfCodeSystem The display name of the code system to use.
   */
  public static void setCodeSystem(String codeSystemOID, String diplayNameOfCodeSystem) {
    defaultCodeSystemOID = codeSystemOID;
    defaultCodeSystemDisplayName = diplayNameOfCodeSystem;
  }

  /** Define a new measurement of value 'd' and unit (using
   * UCUM string) for a measurement of physical quantity
   * defined by the code in the given code system with
   * the given displayName.
   * 
   * @param when The time when the measurement was taken.
   * @param completed Boolean stating whether the measurement was completed correctly.
   * @param context An optional context for the measurement. 
   * @param comment An optional comment.
   */
  public Measurement(Date when, boolean completed, Context context, Comment comment) {
    if (defaultCodeSystemOID == null || defaultCodeSystemDisplayName == null) {
      throw new IllegalStateException("No code system defined for the measurements. Use static method 'setCodeSystem' before creating instances.");
    }
    this.codeSystemOID = defaultCodeSystemOID;
    this.codeSystemDisplayName = defaultCodeSystemDisplayName;
    this.timestamp = when;
    this.completed = completed;
    this.context = context;
    this.comment = comment;
  }

  /** Get the code system OID.
   * @return The code system OID.
   */
  public final String getCodeSystem() {
    return codeSystemOID;
  }

  /** Get the code system name.
   * @return The code system name.
   */
  public final String getCodeSystemName() {
    return codeSystemDisplayName;
  }

  /** Get the measurement time.
   * @return The measurement time.
   */
  public final Date getTimestamp() {
    return timestamp;
  }

  /** Get whether the measurement is completed.
   * @return Boolean stating whether measurement is completed.
   */
  public final boolean getCompleted() {
    return completed;
  }

  /** Set whether the measurement is completed.
   * @param completed Boolean stating whether measurement is completed.
   */
  public final void setCompleted(boolean completed) {
    this.completed = completed;
  }

  /** Get the context.
   * @return The context.
   */
  public final Context getContext() {
    return context;
  }

  /** Set the context.
   * @param context The context.
   */
  public final void setContext(Context context) {
    this.context = context;
  }

  /** Get the comment.
   * @return The comment.
   */
  public final Comment getComment() {
    return comment;
  }

  /** Set the comment.
   * @param comment The comment.
   */
  public final void setComment(Comment comment) {
    this.comment = comment;
  }

  /** Get whether there is a comment.
   * @return Boolean stating whether there is a comment.
   */
  public final boolean hasComment() {
    return this.comment != null;
  }
}
