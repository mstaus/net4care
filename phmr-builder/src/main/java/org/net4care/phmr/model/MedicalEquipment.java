package org.net4care.phmr.model;

/** Medical equipment class. * 
 */
public final class MedicalEquipment {
    private String medicalDeviceCode;
    private String medicalDeviceDisplayName;
    private String manufacturerModelName;
    private String softwareName;

    /** Medical equipment constructor.
     * 
     * @param deviceCode The device code.
     * @param deviceDisplayName The device display name.
     * @param modelName The model name.
     * @param softwareName The software name.
     */
    public MedicalEquipment(String deviceCode, String deviceDisplayName, String modelName, String softwareName) {
      this.medicalDeviceCode = deviceCode;
      this.medicalDeviceDisplayName = deviceDisplayName;
      this.manufacturerModelName = modelName;
      this.softwareName = softwareName;
    }

    /** Get the medical device code.
     * @return The medical device code.
     */
    public String getMedicalDeviceCode() {
      return medicalDeviceCode;
    }

    /** Get the medical device display name.
     * @return The medical device display name.
     */
    public String getMedicalDeviceDisplayName() {
      return medicalDeviceDisplayName;
    }

    /** Get the manufacturer model name.
     * @return The manufacturer model name.
     */
    public String getManufacturerModelName() {
      return manufacturerModelName;
    }

    /** Get the software name.
     * @return The software name.
     */
    public String getSoftwareName() {
      return softwareName;
    }
}
