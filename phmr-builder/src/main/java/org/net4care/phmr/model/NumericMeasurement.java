package org.net4care.phmr.model;

import java.util.Date;

/** Numerical measurement class.
 */
public final class NumericMeasurement extends Measurement {

  private String code;
  private String displayName;
  private String value;
  private String unitString;

  /** Numeric measurement constructor.
   * 
   * @param code The code.
   * @param displayName The display name.
   * @param value The value.
   * @param unitString The unit string.
   * @param when The time of the measurement.
   * @param completed Boolean stating whether it is completed.
   * @param context An optional context.
   * @param comment An optional comment.
   */
  public NumericMeasurement(String code, String displayName, String value, String unitString, 
      Date when, boolean completed, Context context, Comment comment) {
    super(when, completed, context, comment);
    this.code = code;
    this.displayName = displayName;
    this.value = value;
    this.unitString = unitString;
  }

  /** Get the code.
   * @return The code.
   */
  public String getCode() {
    return code;
  }

  /** Get the display name.
   * @return The display name.
   */
  public String getDisplayName() {
    return displayName;
  }

  /** Get the value.
   * @return The value.
   */
  public String getValue() {
    return value;
  }

  /** Get the unit.
   * @return The Unit.
   */
  public String getUnit() {
    return unitString;
  }
}
