package org.net4care.phmr.model;

/** Data structure for information of an organization.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */

public final class OrganizationIdentity implements Locateable {

  private String orgOID;
  private String orgName;
  private Telecom[] telecoms;
  private AddressData address;

  /** Orgnization identity constructor.
   * 
   * @param orgOID Organization OID.
   * @param orgName Organization name.
   * @param telecoms A list of telecoms
   * @param addressData Address data.
   */
  public OrganizationIdentity(String orgOID, String orgName, 
      Telecom[] telecoms, AddressData addressData) {
    this.orgOID = orgOID;
    this.orgName = orgName;
    this.telecoms = telecoms;
    this.address = addressData;

    if (this.telecoms == null) {
      this.telecoms = new Telecom[] {};
    }
  }

  /** Get the organization OID.
   * @return The organization OID.
   */
  public String getOrgOID() {
    return orgOID;
  }

  /** Get the organization name.
   * @return The organization name.
   */
  public String getOrgName() {
    return orgName;
  }

  @Override
  public Telecom[] getTelecomList() {
    return telecoms;
  }

  @Override
  public AddressData getAddress() {
    return address;
  }
}
