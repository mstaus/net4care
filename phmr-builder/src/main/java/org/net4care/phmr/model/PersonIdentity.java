package org.net4care.phmr.model;

import java.util.Date;

/** Datastructure to contain information about a person.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class PersonIdentity implements Locateable {

  /** Gender enumeration. */
  public enum Gender {
    /** Female gender. */
    Female,
    /** Male gender. */
    Male,
    /** Undifferentiated gender. */
    Undifferentiated
  }

  private String ssn;
  private String prefix;
  private String[] givenNames;
  private String familyName;
  private Gender gender;
  private Date birthTime;

  private AddressData address;

  private Telecom[] telecom;

  /** Construct a person identity with complete information.
   * 
   * @param ssn Social Security Number.
   * @param prefix Prefix, e.g. Mr, Ms, or Dr.
   * @param firstNameList A list of first or given names.
   * @param familyName The family name.
   * @param gender The gender as Male, Female, or Undifferentiated. 
   * @param birthTime The birth time.
   * @param address The address as the AddressData type.
   * @param telecoms A list of Telecoms.
   */
  public PersonIdentity(String ssn, 
    String prefix, 
    String[] firstNameList, 
    String familyName, 
    Gender gender, 
    Date birthTime, 
    AddressData address, 
    Telecom[] telecoms) { 
    this.ssn = ssn; 
    this.prefix = prefix;
    this.givenNames = firstNameList; 
    this.familyName = familyName; 
    this.gender = gender; 
    this.birthTime = birthTime; 
    this.address = address; 
    this.telecom = telecoms; 
  } 

  /** Construct a partial person identity, only first names and
   * familyName name, used in e.g. legal authentication
   * 
   * @param firstNameList A list of given names.
   * @param familyName The family name.
   */
  public PersonIdentity(String[] firstNameList, String familyName) {
    // TODO: Introduce some validation in PHMR builder in case a
    // partially defined person is used in e.g. a patient context
    this(null, null, firstNameList, familyName, null, null, null, null); 
  }

  /** Construct a partial person identity, only prefix, first names and
   * familyName name, used in e.g. legal authentication
   * 
   * @param prefix Optional person prefix.
   * @param firstNameList The first name list.
   * @param familyName The family name.
   */
  public PersonIdentity(String prefix, String[] firstNameList, String familyName) {
    // TODO: Introduce some validation in PHMR builder in case a
    // partially defined person is used in e.g. a patient context
    this(null, prefix, firstNameList, familyName, null, null, null, null); 
  }

  /** Get the social security number.
   * @return The social security number.
   */
  public String getSSN() { 
    return ssn; 
  } 
 
  /** Get whether person has a prefix.
   * @return Boolean stating whether there is a prefix.
   */
  public boolean hasPrefix() { 
    return prefix != null && prefix.length() > 0; 
  } 

  /** Get person prefix.
   * @return prefix string.
   */
  public String getPrefix() { 
    return prefix; 
  } 

  /** Get list of given names.
   * @return list of given names.
   */
  public String[] getGivenNames() { 
    return givenNames; 
  } 
 
  /** Get family name.
   * @return family name string.
   */
  public String getFamilyName() { 
    return familyName; 
  } 
 
  /** Get person gender.
   * @return gender type.
   */
  public Gender getGender() { 
    return gender; 
  } 
 
  /** Get person birth time.
   * @return birth time date.
   */
  public Date getBirthTime() { 
    return birthTime; 
  } 
 
  /** Get person address.
   * @return address data.
   */
 @Override
  public AddressData getAddress() { 
    return address; 
  } 

 /** Get person telecom.
  * @return telecom list.
  */
  @Override
  public Telecom[] getTelecomList() { 
    return telecom; 
  } 
 }
