package org.net4care.phmr.model;

import java.util.Date;
import java.util.List;

/** PhmrBuilder interface for building representations of a SimpleClinicalDocument.
 * 
 * Used by the 'construct' method of a SimpleClinicalDocument.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public interface PhmrBuilder {

  /** Build the root node.
   */
  void buildRootNode();

  /** Build the header.
   */
  void buildHeader();

  /** Build the context section. 
   * 
   * @param patientIdentity The person identity.
   * @param effectiveTime The effective time.
   * @param setId The set identifier.
   * @param versionNumber The version number. 
   */
  void buildContext(PersonIdentity patientIdentity, Date effectiveTime, String setId, String versionNumber);

  /** Build the patient section.
   * @param patientIdentity The patient identity.
   */
  void buildPatientSection(PersonIdentity patientIdentity);

  /** Build the result measurements.
   * @param results The list of results to build.
   */
  void buildResults(List<Measurement> results);

  /** Build the vital signs measurements.
   * @param vitalSigns The list of vital signs to build.
   */
  void buildVitalSigns(List<Measurement> vitalSigns);

  /** Build structured body section.
   */
  void buildStructuredBodySection();

  /** Build author section.
   * 
   * @param authorOrganizationIdentity The author organization identity.
   * @param authorIdentity The author person identity
   * @param authorParticipationStartTime The author participation start time.
   */
  void buildAuthorSection(
      OrganizationIdentity authorOrganizationIdentity, 
      PersonIdentity authorIdentity, 
      Date authorParticipationStartTime);

  /** Build custodian section.
   * 
   * @param custodianIdentity The custodian person identity.
   */
  void buildCustodianSection(OrganizationIdentity custodianIdentity);

  /** Build legal authenticator section.
   * 
   * @param authenticatorOrganizationIdentity The authenticator organization identity. 
   * @param authenticatorPersonIdentity The authenticator person identity. 
   * @param timeOfAuthentication The time of authentication.
   */
  void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity,
      Date timeOfAuthentication);

  /** Build documentation section.
   *
   * @param serviceStart The time of service start.
   * @param serviceEnd The time of servoce end.
   */
  void buildDocumentationOf(Date serviceStart, Date serviceEnd);

  /** Build medical equipment section.
   * 
   * @param equipments A list of equipment.
   */
  void buildMedicalEquipmentSection(List<MedicalEquipment> equipments);
}
