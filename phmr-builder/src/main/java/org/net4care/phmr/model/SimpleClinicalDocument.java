package org.net4care.phmr.model;

import java.util.Date;

/** SimpleClinicalDocument is inspired by the GreenCDA project that
 * focus on 'dynamic fields' (user data) while avoiding 'static fields'
 * (boilerplate data, like template ids, typeCodes, etc) of the CDA.
 * 
 * A SimpleClinicalDocument is a Java data object
 * that contains the business related data for telemedicine, like
 * patient information, measurements, device information, custodian
 * organization, etc.
 * 
 * To produce valid PersonalHealthMonitoringRecord XML documents
 * you use the 'construct' method with a suitable PHMRBuilder instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public interface SimpleClinicalDocument {

  /** Define when the document was created. 
   * 
   * @param documentCreationTime The document creation time.
   */
  void setEffectiveTime(Date documentCreationTime);

  /** Define a unique identifier (CDA Release 2, §4.2.1.7 ClinicalDocument.setId /
   * "Represents an identifier that is common across all document revisions")
   * and the version number (CDA Release 2, §4.2.1.8 ClinicalDocument.versionNumber /
   * "An integer value used to version successive replacement documents".
   * Normally for PHMR documents just use a unique identifier, and version number
   * 1.
   * @param uniqueId the identifier for the document
   * @param versionNumber the number of this version
   */
  void setDocumentVersion(String uniqueId, int versionNumber);

  /** Define who is the patient.
   * 
   *  @param patientIdentity The patient identity as a PersonIdentity.
   *  */
  void setPatient(PersonIdentity patientIdentity);

  /** Define who is the author of the document, i.e.
   * the organization and person and time when
   * the author participated in creating the document.
   * @param authorOrganizationIdentity The author organization identity.
   * @param authorPersonIdentity The author person identity.
   * @param authorParticipationStart The author participation start.
   */
  void setAuthor(OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorPersonIdentity,
      Date authorParticipationStart);

  /** Set the organization that is custodian of the document.
   * 
   *  @param custodianIdentity The custodian organization identity.
   *  */
  void setCustodian(OrganizationIdentity custodianIdentity);

  /** Set the organization, person, and time of authentication.
   * 
   *  @param authenticatorOrganizationIdentity The authenticator organization identity.
   *  @param authenticatorPersonIdentity The authenticator person identity.
   *  @param timeOfAuthentication The time of authentication.
   *  */
  void setAuthenticator(OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication);

  /** Set the time interval that the measurements span.
   * 
   *  @param from The earliest time in the time interval.
   *  @param to The latest time in the time interval.
   *  */
  void setDocumentationTimeInterval(Date from, Date to);

  /** Add the medical equipment used for making the measurements.
   * 
   *  @param medicalDeviceCode The medical device code.
   *  @param medicalDeviceDisplayName The medical device display name.
   *  @param manufacturerModelName The manufactorer model name.
   *  @param softwareName The software name.
   *  */
  void addMedicalEquipment(String medicalDeviceCode, String medicalDeviceDisplayName,
      String manufacturerModelName, String softwareName);

  /** Add the medical equipment used for making the measurements.
   * 
   *  @param equipment The medical equipment.
   *  */
  void addMedicalEquipment(MedicalEquipment equipment);

  /** Add a result measurement. 
   * 
   * @param measurement The result measurement to add.
   */
  void addResult(Measurement measurement);

  /** Add a vital sign measurement. 
   * 
   * @param measurement The vital sign measurement to add.
   */
  void addVitalSign(Measurement measurement);

  /** Given a builder instance, construct a representation of this object.
   * 
   * @param builder A valid builder instance
   */
  void construct(PhmrBuilder builder);
}
