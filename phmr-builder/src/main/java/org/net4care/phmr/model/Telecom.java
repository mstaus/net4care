package org.net4care.phmr.model;

/** Data structure for a single telecom instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class Telecom {

  private String value;
  private AddressData.Use use;

  /** Telecom constructor.
   * @param use The address use of the telecom.
   * @param value The telecom value.
   */
  public Telecom(AddressData.Use use, String value) {
    this.value = value;
    this.use = use;
  }

  /** Get the telecom value.
   * @return the telecom value.
   */
  public String getValue() {
    return value;
  }

  /** Get the address use.
   * @return the address use.
   */
  public AddressData.Use getAddressUse() {
    return use;
  }
}
