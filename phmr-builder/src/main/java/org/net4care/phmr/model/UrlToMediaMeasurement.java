package org.net4care.phmr.model;

import java.util.Date;

/** URL to media measurement. */
public final class UrlToMediaMeasurement extends Measurement {

  private String id;
  private String reference;

  /** The URL to media measurement constructor.
   * 
   * @param id The reference id.
   * @param reference The reference.
   * @param when The time of the measurement.
   * @param completed Boolean stating whether it is completed.
   * @param context An optional context.
   * @param comment An optional comment.
   */
  public UrlToMediaMeasurement(String id, String reference, 
      Date when, boolean completed, Context context, Comment comment) {
    super(when, completed, context, comment);
    this.id = id;
    this.reference = reference;
  }

  /** Get the reference id.
   * @return The reference id.
   */
  public String getId() {
    return id;
  }

  /** Get the reference.
   * @return The reference.
   */
  public String getReference() {
    return reference;
  }
}
