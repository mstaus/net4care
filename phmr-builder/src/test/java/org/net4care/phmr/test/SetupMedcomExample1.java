package org.net4care.phmr.test;

import java.util.Date;

import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.AddressData;
import org.net4care.phmr.model.Context;
import org.net4care.phmr.model.DanishPHMRModel;
import org.net4care.phmr.model.Measurement;
import org.net4care.phmr.model.NumericMeasurement;
import org.net4care.phmr.model.OrganizationIdentity;
import org.net4care.phmr.model.PersonIdentity;
import org.net4care.phmr.model.SimpleClinicalDocument;
import org.net4care.phmr.model.Telecom;

public class SetupMedcomExample1 {

  /** Define a CDA for the Medcom example 1. */
  public static SimpleClinicalDocument defineAsCDA() {

    // Define NPU as the global code system to use for all measurements as the default
    Measurement.setCodeSystem(NPU.CODESYSTEM_OID, NPU.DISPLAYNAME);

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity(new String[] {"Anders"}, "Andersen");

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1);
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    PersonIdentity nancy = new PersonIdentity("2512484916", 
      "", new String[]{"Nancy", "Ann"}, "Berggren", 
      PersonIdentity.Gender.Female, 
      HelperMethods.makeUtcDate(1948, 11, 25),
      new AddressData(AddressData.Use.HomeAddress,
        new String[]{"Skovvejen 12", "Landet"}, "5700", "Svendborg", 
        "Danmark"), new Telecom[] { 
      new Telecom(AddressData.Use.HomeAddress, "tel:65123456"),
      new Telecom(AddressData.Use.WorkPlace, "mailto:nab@udkantsdanmark.dk") }
   );
    cda.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity svendborgHjerteMedicinskAfdeling  = 
      new OrganizationIdentity("88878685",
        "Odense Universitetshospital - Svendborg Sygehus",
        new Telecom[]{new Telecom(AddressData.Use.WorkPlace, "tel:65223344")},
        new AddressData(AddressData.Use.WorkPlace,
          new String[]{"Hjertemedicinsk afdeling B", "Valdemarsgade 53"},
          "5700", "Svendborg", "Danmark"));
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, andersAndersen, documentCreationTime);
    cda.setCustodian(svendborgHjerteMedicinskAfdeling);
    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    cda.setAuthenticator(svendborgHjerteMedicinskAfdeling, andersAndersen, at1000onJan13);

    // 1.4 Define the service period
    Date from = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    cda.setDocumentationTimeInterval(from, to);

    // 1.5 Add measuring equipment
    cda.addMedicalEquipment("EPQ12225", "Weight",
      "Manufacturer: AD Company / Model: 6121ABT1",
      "SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711");

    // 1.6 Add measurements (observations)

    // Example 1: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Date time1 = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Context context = new Context(Context.ProvisionMethod.Electronicallly, Context.PerformerType.Citizen, null);
    Measurement weight1 = NPU.createWeight("77.5", time1, context);
    cda.addResult(weight1);

    // Use the basic methods that allow any legal
    // code system to be used but requires all data to be
    // provided
    Date time2 = HelperMethods.makeDanishDateTime(2014, 0, 8, 7, 45, 0);
    Measurement weight2 = new NumericMeasurement("NPU03804", "Legeme masse; Pt", "77.0", "kg", time2, true, context, null);
    cda.addResult(weight2);

    Date time3 = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    Measurement weight3 = new NumericMeasurement("NPU03804", "Legeme masse; Pt", "77.2", "kg", time3, true, context, null);
    cda.addResult(weight3);

    return cda;
  }
}
