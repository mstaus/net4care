package org.net4care.phmr.test;

import java.util.Date;

import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.AddressData;
import org.net4care.phmr.model.Comment;
import org.net4care.phmr.model.Context;
import org.net4care.phmr.model.DanishPHMRModel;
import org.net4care.phmr.model.Measurement;
import org.net4care.phmr.model.OrganizationIdentity;
import org.net4care.phmr.model.PersonIdentity;
import org.net4care.phmr.model.SimpleClinicalDocument;
import org.net4care.phmr.model.Telecom;

public class SetupMedcomExample2 {

  /** Define a CDA for the Medcom example 2. */
  public static SimpleClinicalDocument defineAsCDA() {

    // Define NPU as the global code system to use for all measurements as the default
    Measurement.setCodeSystem(NPU.CODESYSTEM_OID, NPU.DISPLAYNAME);

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 21, 10, 30, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1);
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information (from base)
    PersonIdentity nancy = new PersonIdentity("2512484916", 
      "", new String[] {"Nancy", "Ann"}, "Berggren", 
      PersonIdentity.Gender.Female, 
      HelperMethods.makeUtcDate(1948, 11, 25),
      new AddressData(AddressData.Use.HomeAddress,
        new String[] {"Skovvejen 12", "Landet"}, "5700", "Svendborg", "Danmark"), 
        new Telecom[] {
          new Telecom(AddressData.Use.HomeAddress, "tel:65123456"),
          new Telecom(AddressData.Use.WorkPlace, "mailto:nab@udkantsdanmark.dk")}
       );
    cda.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    PersonIdentity andersAndersen = new PersonIdentity("Hjertelæge", new String[] {"Anders"}, "Andersen");
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity svendborgHjerteMedicinskAfdeling  = 
      new OrganizationIdentity("88878685",
        "Odense Universitetshospital - Svendborg Sygehus",
        new Telecom[] {new Telecom(AddressData.Use.WorkPlace, "tel:65223344")},
        new AddressData(AddressData.Use.WorkPlace,
          new String[] {"Hjertemedicinsk afdeling B", "Valdemarsgade 53"},
          "5700", "Svendborg", "Danmark"));
    PersonIdentity mathildeChristesen = new PersonIdentity("Hjemmesygeplejerske", new String[] {"Mathilde"}, "Christensen");
    OrganizationIdentity hjemmeplejen = 
      new OrganizationIdentity("328151000016009",
        "Hjemmesygeplejen, Svendborg kommune", null,
        new AddressData(AddressData.Use.WorkPlace,
          new String[] {"Svinget 14"},
          "5700", "Svendborg", "Danmark"));
    Date timeOfAuthor = HelperMethods.makeDanishDateTime(2014, 0, 24, 7, 53, 0);
    cda.setAuthor(hjemmeplejen, mathildeChristesen, timeOfAuthor);
    cda.setCustodian(svendborgHjerteMedicinskAfdeling);
    Date timeOfAuthentication = HelperMethods.makeDanishDateTime(2014, 0, 21, 10, 30, 0);
    cda.setAuthenticator(svendborgHjerteMedicinskAfdeling, andersAndersen, timeOfAuthentication);

    // 1.4 Define the service period
    Date fromTime = HelperMethods.makeDanishDateTime(2014, 0, 20, 7, 53, 0);
    Date toTime = HelperMethods.makeDanishDateTime(2014, 0, 20, 14, 25, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    cda.addMedicalEquipment("EPQXXXXX", "Weight",
      "Manufacturer: AD Company / Model: 6121ABT1",
      "SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711");

    cda.addMedicalEquipment("EPQXXXXX", "Blood Pressure Monitor",
      "Manufacturer: AD Company / Model: AU-767PBT-C",
      "SerialNr: AU-767PBT-C Rev. 2 / SW Rev. 45144723");

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Context context = new Context(Context.ProvisionMethod.TypedByHealthcareProfessional, Context.PerformerType.HealthcareProfessional, null);
    Comment comment = new Comment(andersAndersen, svendborgHjerteMedicinskAfdeling, HelperMethods.makeDanishDateTime(2014, 0, 21, 10, 40, 0),
      "Jeg kan se at denne måling er tastet ind af Hjemmesygeplejerske Mathilde Christensen. " 
      + "Har mistanke om, at den høje værdi skyldes en indtastningsfejl. Målingen kan ikke godkendes. AA");
    Measurement systolic = NPU.createBloodPresureSystolic("253", toTime, context);
    systolic.setCompleted(false);
    systolic.setComment(comment);
    cda.addVitalSign(systolic);

    Measurement diastolic = NPU.createBloodPresureDiastolic("86", toTime, context);
    diastolic.setCompleted(false);
    cda.addVitalSign(diastolic);

    context = new Context(Context.ProvisionMethod.Electronicallly, Context.PerformerType.Citizen, null);
    Measurement weight = NPU.createWeight("77.3", fromTime, context);
    cda.addResult(weight);

    return cda;
  }
}
