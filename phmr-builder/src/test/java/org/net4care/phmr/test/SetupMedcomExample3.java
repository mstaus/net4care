package org.net4care.phmr.test;

import java.util.Date;

import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.AddressData;
import org.net4care.phmr.model.Comment;
import org.net4care.phmr.model.Context;
import org.net4care.phmr.model.DanishPHMRModel;
import org.net4care.phmr.model.Measurement;
import org.net4care.phmr.model.OrganizationIdentity;
import org.net4care.phmr.model.PersonIdentity;
import org.net4care.phmr.model.SimpleClinicalDocument;
import org.net4care.phmr.model.Telecom;

public class SetupMedcomExample3 {

  /** Define a CDA for the Medcom example 3. */
  public static SimpleClinicalDocument defineAsCDA() {

    // Define NPU as the global code system to use for all measurements as the default
    Measurement.setCodeSystem(NPU.CODESYSTEM_OID, NPU.DISPLAYNAME);

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 14, 10, 0, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1);
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information (from base)
    PersonIdentity nancy = new PersonIdentity("2105669996", 
      "", new String[]{"Ellen"}, "Berggren", 
      PersonIdentity.Gender.Female, 
      HelperMethods.makeUtcDate(1966, 4, 21),
      new AddressData(AddressData.Use.HomeAddress,
        new String[] {"Martin Bachs Vej 3"}, "8850", "Bjerringbro", "Danmark"), 
        new Telecom[] { 
          new Telecom(AddressData.Use.HomeAddress, "tel:89123456"),
          new Telecom(AddressData.Use.WorkPlace, "mailto:eb@udkantsdanmark.dk") }
       );
    cda.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    OrganizationIdentity custodian = 
      new OrganizationIdentity("88878685",
        "Århus Universitetshospital", 
        new Telecom[]{new Telecom(AddressData.Use.WorkPlace, "tel:78450000")},
        new AddressData(AddressData.Use.WorkPlace,
          new String[]{"Afdelingen for Obstetrik og Gynækologi", "Brendstrupgårdsvej 100"},
          "8200", "Århus", "Danmark"));
    PersonIdentity author = new PersonIdentity("Jordemoder", new String[] {"Sarah"}, "Andersen");
    PersonIdentity authenticator = new PersonIdentity(new String[] {"Peter"}, "Petersen");

    cda.setAuthor(custodian, author, documentCreationTime);
    cda.setCustodian(custodian);
    cda.setAuthenticator(custodian, authenticator, documentCreationTime);

    // 1.4 Define the service period
    Date fromTime = HelperMethods.makeDanishDateTime(2014, 0, 14, 9, 45, 0);
    Date toTime = HelperMethods.makeDanishDateTime(2014, 0, 14, 10, 0, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    cda.addMedicalEquipment("EPQXXXXX", "Weight",
      "Manufacturer: AD Company / Model: 6121ABT1",
      "SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711");

    cda.addMedicalEquipment("EPQXXXXX", "sphygmomanometer",
      "Manufacturer: AD Company / Model: AU-767PBT-C",
      "SerialNr: AU-767PBT-C Rev. 2 / SW Rev. 45144723");

    cda.addMedicalEquipment("EPQXXXXX", "Urine Analyzer",
      "Manufacturer: Roche Diagnostics / Model: Urisys 1100",
      "SerialNr: GD-226789-F Rev. 4.7 / SW Rev. 23980");

    cda.addMedicalEquipment("EPQXXXXX", "Fetal Monitor (Monica)",
        "Manufacturer: GE Healthcare / Model: Corometrics 170",
        "SerialNr: GE-114567CRM-C Rev. 4 / SW Rev. 743214");

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Context context = new Context(Context.ProvisionMethod.Electronicallly, Context.PerformerType.Citizen, null);

    Measurement systolic = NPU.createBloodPresureSystolic("138", fromTime, context);
    cda.addVitalSign(systolic);

    Measurement diastolic = NPU.createBloodPresureDiastolic("91", fromTime, context);
    cda.addVitalSign(diastolic);

    Measurement weight = NPU.createWeight("75.0", fromTime, context);
    cda.addResult(weight);

    Measurement protein = NPU.createProteinUrine("0", fromTime, context);
    cda.addResult(protein);

    Comment comment = new Comment(author, custodian, toTime, "CTG-målingen ser helt normal ud, intet at bemærke. SA");
    Measurement ctg = NPU.createCTG("484ff720-8f2c-11e3-baa8-0800200c9a66", "ctg_obs/484ff720-8f2c-11e3-baa8-0800200c9a66.png", fromTime, null);
    ctg.setComment(comment);
    cda.addResult(ctg);

    return cda;
  }
}
