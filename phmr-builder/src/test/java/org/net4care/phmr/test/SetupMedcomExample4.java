package org.net4care.phmr.test;

import java.util.Date;

import org.net4care.phmr.codes.DAK;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.AddressData;
import org.net4care.phmr.model.Context;
import org.net4care.phmr.model.DanishPHMRModel;
import org.net4care.phmr.model.Measurement;
import org.net4care.phmr.model.OrganizationIdentity;
import org.net4care.phmr.model.PersonIdentity;
import org.net4care.phmr.model.SimpleClinicalDocument;
import org.net4care.phmr.model.Telecom;

public class SetupMedcomExample4 {

  /** Define a CDA for the Medcom example 4. */
  public static SimpleClinicalDocument defineAsCDA() {

    // Define NPU as the global code system to use for all measurements as the default
    Measurement.setCodeSystem(NPU.CODESYSTEM_OID, NPU.DISPLAYNAME);

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2013, 01, 17, 9, 15, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    SimpleClinicalDocument cda = new DanishPHMRModel();

    // 1.1 Populate with time and version info
    cda.setDocumentVersion("2358344", 1); // TODO: move to codes
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information (from base)
    PersonIdentity nancy = new PersonIdentity("2606481234", 
      "", new String[]{"Janus"}, "Berggren", 
      PersonIdentity.Gender.Male,
      HelperMethods.makeUtcDate(1948, 5, 26),
      new AddressData(AddressData.Use.HomeAddress,
        new String[] {"Skovvejen 12", "Landet"}, "5700", "Svendborg", "Danmark"), 
        new Telecom[] { 
          new Telecom(AddressData.Use.HomeAddress, "tel:65123456"),
          new Telecom(AddressData.Use.WorkPlace, "mailto:jab@udkantsdanmark.dk") }
       );
    cda.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    OrganizationIdentity custodian = 
      new OrganizationIdentity("77668685",
        "Odense Universitetshospital", 
        null, // new Telecom[]{new Telecom(AddressData.Use.WorkPlace, "tel:78450000")},
        new AddressData(AddressData.Use.WorkPlace,
          new String[]{"Lungemedicinsk afdeling J", "Sdr. Boulevard 29,", "Indgang 87-88" },
          "5000", "Odense C", "Danmark"));
    PersonIdentity author = new PersonIdentity("Læge", new String[] {"M"}, "Madsen");
    PersonIdentity authenticator = new PersonIdentity("Læge", new String[] {"Lars"}, "Olsen");

    cda.setAuthor(custodian, author, documentCreationTime);
    cda.setCustodian(custodian);
    cda.setAuthenticator(custodian, authenticator, documentCreationTime);

    // 1.4 Define the service period
    Date fromTime = HelperMethods.makeDanishDateTime(2013, 1, 11, 9, 11, 0);
    Date toTime = HelperMethods.makeDanishDateTime(2013, 1, 16, 11, 14, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    cda.addMedicalEquipment("EPQXXXXX", "COPD Kit",
      "Manufacturer: AD Company / Model: COHM-0034",
      "SerialNr: 6788-0034987 / SW Rev. 6755-ABT");

    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Context context = new Context(Context.ProvisionMethod.Electronicallly, Context.PerformerType.Citizen, null);

    cda.addVitalSign(NPU.createSaturation("92", HelperMethods.makeDanishDateTime(2013, 1, 11, 9, 11, 0), context));
    cda.addVitalSign(NPU.createSaturation("91", HelperMethods.makeDanishDateTime(2013, 1, 12, 8, 45, 0), context));
    cda.addVitalSign(NPU.createSaturation("94", HelperMethods.makeDanishDateTime(2013, 1, 13, 9, 15, 0), context));
    cda.addVitalSign(NPU.createSaturation("95", HelperMethods.makeDanishDateTime(2013, 1, 14, 9, 45, 0), context));
    cda.addVitalSign(NPU.createSaturation("90", HelperMethods.makeDanishDateTime(2013, 1, 15, 7, 1, 0), context));
    cda.addVitalSign(NPU.createSaturation("94", HelperMethods.makeDanishDateTime(2013, 1, 16, 11, 10, 0), context));

    // use DAK-E code system
    Measurement.setCodeSystem(DAK.CODESYSTEM_OID, DAK.DISPLAYNAME);
    cda.addResult(DAK.createFVC("2.5", HelperMethods.makeDanishDateTime(2013, 1, 11, 9, 14, 0), context));
    cda.addResult(DAK.createFVC("2.4", HelperMethods.makeDanishDateTime(2013, 1, 12, 8, 50, 0), context));
    cda.addResult(DAK.createFVC("2.9", HelperMethods.makeDanishDateTime(2013, 1, 13, 9, 21, 0), context));
    cda.addResult(DAK.createFVC("3.1", HelperMethods.makeDanishDateTime(2013, 1, 14, 9, 50, 0), context));
    cda.addResult(DAK.createFVC("2.2", HelperMethods.makeDanishDateTime(2013, 1, 15, 7, 10, 0), context));
    cda.addResult(DAK.createFVC("3.0", HelperMethods.makeDanishDateTime(2013, 1, 16, 11, 14, 0), context));

    return cda;
  }
}
