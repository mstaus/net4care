package org.net4care.phmr.test;

import org.net4care.phmr.builders.UUIDStrategy;

/** A test stub which generates the same, constant, UUID
 * every time to allow automated testing.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class StubUUIDStrategy implements UUIDStrategy {

  private String uuid;

  public StubUUIDStrategy(String uuid) {
    this.uuid = uuid;
  }

  public StubUUIDStrategy() {
    // UUID.randomUUID().toString()
    this("00000000-0000-0000-0000-000000000000");
  }

  @Override
  public String generate() {
    return uuid;
  }
}
