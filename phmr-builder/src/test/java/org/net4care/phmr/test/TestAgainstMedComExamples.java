package org.net4care.phmr.test;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.*;

import org.custommonkey.xmlunit.*;
import org.junit.Test;
import org.net4care.phmr.builders.DanishPHMRBuilder;
import org.net4care.phmr.model.SimpleClinicalDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;

/** Validate the output from our own PHMR builder with
 * the example from MedCom.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class TestAgainstMedComExamples {

  @Test 
  public void shouldMatchExpectedValueEx1() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 1
    SimpleClinicalDocument cda = SetupMedcomExample1.defineAsCDA();

    // Load the MedCom example 1, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "aa2386d0-79ea-11e3-981f-0800200c9a66", "Ex1-Weight_measurement-MODIFIED.xml");
  }

  @Test 
  public void shouldMatchExpectedValueEx2() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 2
    SimpleClinicalDocument cda = SetupMedcomExample2.defineAsCDA();

    // Load the MedCom example 2, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "b6a079b0-89ab-11e3-baa8-0800200c9a66", "Ex2-Typing_error-MODIFIED.xml");
  }

  @Test 
  public void shouldMatchExpectedValueEx3() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 3
    SimpleClinicalDocument cda = SetupMedcomExample3.defineAsCDA();

    // Load the MedCom example 3, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "1b77bde0-9563-11e3-a5e2-0800200c9a66", "Ex3-Preeclampsia-MODIFIED.xml");
  }

  @Test 
  public void shouldMatchExpectedValueEx4() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 4
    SimpleClinicalDocument cda = SetupMedcomExample4.defineAsCDA();

    // Load the MedCom example 4, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "437f8b60-9563-11e3-a5e2-0800200c9a66", "Ex4-COPD-MODIFIED.xml");
  }

  private void shouldMatchExpectedValue(SimpleClinicalDocument cda, String uuid, String filename) throws ParserConfigurationException, SAXException, IOException { 

    DanishPHMRBuilder phmrBuilder = new DanishPHMRBuilder(new StubUUIDStrategy(uuid));

    // MedCom example references a local style sheet
    ProcessingInstruction pi = phmrBuilder.getDocument().createProcessingInstruction("xml-stylesheet", 
        "type=\"text/xsl\" href=\"../Stylesheet/cda.xsl\"");
    phmrBuilder.getDocument().appendChild(pi);

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
    // important! http://www.kdgregory.com/index.php?page=xml.parsing 
    // factory.setNamespaceAware(true);
    DocumentBuilder xmlBuilder = factory.newDocumentBuilder();

    // Load the MedCom example
    // this requires the XML files to have an UTF-8 BOM
    Document expected = xmlBuilder.parse("src/test/resources/" + filename); 

    // Generate the XML
    cda.construct(phmrBuilder);
    // Extract the actual XML DOM and string representation
    Document computed = phmrBuilder.getDocument();

    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expected, computed);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      String computedXml = HelperMethods.convertXMLDocumentToString(computed);
      String expectedXml = HelperMethods.convertXMLDocumentToString(expected);

      System.out.println("------------- Detailed Message --------------");
      System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
      System.out.println("------------- Expected XML ------------------");
      System.out.print(HelperMethods.indentLines(expectedXml));
      System.out.println("------------- Computed XML ------------------");
      System.out.print(HelperMethods.indentLines(computedXml));
      System.out.println("------------- End of details ----------------");
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }
}
