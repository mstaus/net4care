package org.net4care.phmr.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.*;
import org.net4care.phmr.builders.DanishPHMRBuilder;
import org.net4care.phmr.model.*;
import org.w3c.dom.Document;

public final class TestMethodCodeMeasurements {
  private Document phmrAsXML;
  private String asString;

  private SimpleClinicalDocument cda;

  @Before
  public void setup() {
    // Step 1. Define the Medcom EX1 CDA
    cda = SetupMedcomExample1.defineAsCDA();
  }

  @Test
  public void shouldValidateMedComCodesForEx1() {
    Context context = new Context(Context.ProvisionMethod.Electronicallly, Context.PerformerType.Citizen, null);

    asString = buildXMLStringForCDAWithContextMeasurement(context);

    // System.out.println(asString);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af borger\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"AUT\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Måling overført automatisk\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesByHealthcareProfessionals() {
    Context context = new Context(Context.ProvisionMethod.TypedByHealthcareProfessional, Context.PerformerType.HealthcareProfessional, null);

    asString = buildXMLStringForCDAWithContextMeasurement(context);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"PNT\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af aut. sundhedsperson\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPH\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af aut. sundhedsperson\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForCaregiver() {
    Context context = new Context(Context.ProvisionMethod.TypedByCareGiver, Context.PerformerType.CareGiver, null);

    asString = buildXMLStringForCDAWithContextMeasurement(context);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"PCG\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af anden omsorgsperson\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPC\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af anden omsorgsperson\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForRelative() {
    Context context = new Context(Context.ProvisionMethod.TypedByCitizenRelative, Context.PerformerType.Citizen, null);

    asString = buildXMLStringForCDAWithContextMeasurement(context);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af borger\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPR\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af pårørende\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForCitizen() {
    Context context = new Context(Context.ProvisionMethod.TypedByCitizen, Context.PerformerType.Citizen, null);

    asString = buildXMLStringForCDAWithContextMeasurement(context);

    // System.out.println(asString);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Målt af borger\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPD\" codeSystem=\"2.16.840.1.113883.3.4208.100.6\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("codeSystemName=\"MedCom Message Codes\" displayName=\"Indtastet af borger\"/>"));
  }

  private String buildXMLStringForCDAWithContextMeasurement(Context context) {
    Measurement bloodpressure;
    Date when = HelperMethods.makeDanishDateTime(2014, 0, 14, 9, 45, 00);
    bloodpressure = 
        new NumericMeasurement("DNK05472", "Blodtryk systolisk;Arm", "138.0", "mm[Hg]", when, true, context, null);

    cda.addResult(bloodpressure);

    asString = buildXMLStringForCDA(cda);
    return asString;
  }

  private String buildXMLStringForCDA(SimpleClinicalDocument cda) {
    // 2. Convert it into a Danish PHMR XML format
    DanishPHMRBuilder builder = new DanishPHMRBuilder(new StubUUIDStrategy("aa2386d0-79ea-11e3-981f-0800200c9a66"));
    cda.construct(builder);
    // 3. Extract the acutal XML DOM and string representation
    phmrAsXML = builder.getDocument();

    // Convert it to string
    asString = HelperMethods.convertXMLDocumentToString(phmrAsXML);

    return asString;
  }
}
