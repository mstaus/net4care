package org.net4care.phmr.test;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;
import org.net4care.phmr.builders.DanishPHMRBuilder;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.*;
import org.w3c.dom.*;

/** Test to drive implementation of a GreenCDA like
 * Clinical document conforming to the Danish PHMR
 * standard.
 * 
 * Note, this is not a systematic testing effort,
 * but a TDD effort to get the basic abstractions
 * into place. Other tests will validate the
 * exact output with examples from MedCom.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class TestPHMRBuilding {

  private Document phmrAsXML;
  private String asString;

  @Before
  public void setup() {
    // Step 1. Define the Medcom EX1 CDA
    SimpleClinicalDocument cda = SetupMedcomExample1.defineAsCDA();

    // Tweak some fields of the contents to ensure proper testing

    // A) Set effective time to 10:11:12 instead
    cda.setEffectiveTime(HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 11, 12));

    // B)  Setup 'fake' afdeling X to make a difference in the XML that we can spot
    OrganizationIdentity authenticatorOrgIdentity  = 
        new OrganizationIdentity("88878685",
            "Odense Universitetshospital - Svendborg Sygehus X",
            new Telecom[]{new Telecom(AddressData.Use.WorkPlace, "tel:55555555")},
        new AddressData(AddressData.Use.WorkPlace,
            new String[]{"Hjertemedicinsk afdeling X", "Valdemarsgade 53x"},
            "5700", "Svendborg", "Danmark"));

    // Setup Anders Andersen as authenticator
    PersonIdentity authenticatorIdentity =
        new PersonIdentity(new String[] {"Anders"}, "Andersen");

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    cda.setAuthenticator(authenticatorOrgIdentity, authenticatorIdentity, at1000onJan13);

    // 2. Convert it into a Danish PHMR XML format
    DanishPHMRBuilder builder = new DanishPHMRBuilder(new StubUUIDStrategy("aa2386d0-79ea-11e3-981f-0800200c9a66"));
    cda.construct(builder);
    // 3. Extract the acutal XML DOM and string representation
    phmrAsXML = builder.getDocument();

    // Convert it to string
    asString = HelperMethods.convertXMLDocumentToString(phmrAsXML);
    // System.out.println(asString);
  }


  /* Smoke test the header */
  @Test
  public void shouldValidateHeader() {
    assertNotNull("The phmr document is null.", phmrAsXML);

    assertTrue("PHMR has not the proper title", 
        asString.contains("<title>Hjemmemonitorering for 2512484916</title>"));
    assertTrue("PHMR has not the DK templateID", 
        asString.contains("<templateId root=\"2.16.840.1.113883.3.4208.11.1\"/>"));

    assertTrue("PHMR has not the proper id", 
        asString.contains("<id assigningAuthorityName=\"MedCom\" extension=\"aa2386d0-79ea-11e3-981f-0800200c9a66\" root=\"2.16.840.1.113883.3.4208\"/>"));

    assertTrue("typeId / root is incorrect",
        asString.contains("root=\"2.16.840.1.113883.1.3\""));

    assertTrue("effectiveTime is missing or incorrectly formatted", 
        asString.contains("<effectiveTime value=\"20140113101112+0100\"/>"));

    assertTrue("confidentialityCode missing or wrong",
        asString.contains("<confidentialityCode code=\"N\" codeSystem=\"2.16.840.1.113883.5.25\"/>"));

    assertTrue("language code missing or wrong",
        asString.contains("<languageCode code=\"da-DK\"/>"));

    assertTrue("setId missing or wrong",
        asString.contains("<setId extension=\"2358344\" root=\"2.16.840.1.113883.3.4208.100.6\"/>"));

    assertTrue("versionNumber missing or wrong",
        asString.contains("<versionNumber value=\"1\"/>"));
  }

  @Test
  public void shouldValidateRecordTarget() {
    assertTrue("recordTarget missing or wrong",
        asString.contains("<recordTarget contextControlCode=\"OP\" typeCode=\"RCT\">"));
    assertTrue("patientRole missing or wrong",
        asString.contains("<patientRole classCode=\"PAT\">"));

    assertEquals("CPR missing in patientRole",
        "2512484916", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("extension", 0, "id", "patientRole", phmrAsXML));

    assertTrue("name tag missing or wrong",
        asString.contains("<name>"));

    assertTrue("Nancy missing or wrong",
        asString.contains("<given>Nancy</given>"));
    assertTrue("Ann missing or wrong",
        asString.contains("<given>Ann</given>"));
    assertTrue("Berggren missing or wrong",
        asString.contains("<family>Berggren</family>"));

    assertTrue("Gender missing or wrong",
        asString.contains("<administrativeGenderCode code=\"F\" codeSystem=\"2.16.840.1.113883.5.1\"/>"));

    assertTrue("Birthtime missing or wrong",
        asString.contains("<birthTime value=\"19481225\"/>"));

    assertTrue("addr missing or wrong",
        asString.contains("<addr use=\"H\">"));
    assertTrue("Streetline 1 missing or wrong",
        asString.contains("<streetAddressLine>Skovvejen 12</streetAddressLine>"));
    assertTrue("Streetline 2 missing or wrong",
        asString.contains("<streetAddressLine>Landet</streetAddressLine>"));

    assertTrue("postalCode missing or wrong",
        asString.contains("<postalCode>5700</postalCode>"));
    assertTrue("city missing or wrong",
        asString.contains("<city>Svendborg</city>"));

    assertTrue("telecom 1 missing or wrong",
        asString.contains("<telecom use=\"H\" value=\"tel:65123456\"/>"));
    assertTrue("telecom 2 missing or wrong",
        asString.contains("<telecom use=\"WP\" value=\"mailto:nab@udkantsdanmark.dk\"/>"));

    // assertTrue("confidentialityCode missing or wrong", asString.contains(""));
    // assertTrue("confidentialityCode missing or wrong", asString.contains(""));
  }

  @Test
  public void shouldValidateAuthor() {
    assertTrue("author missing or wrong",
        asString.contains("<author contextControlCode=\"OP\" typeCode=\"AUT\">"));
    // Further validation in the 'compare xml' test case.
  }

  @Test
  public void shouldValidateCustodian() {
    assertTrue("custodian missing or wrong",
        asString.contains("<custodian typeCode=\"CST\">"));
    assertTrue("assignedCustodian missing or wrong",
        asString.contains("<assignedCustodian classCode=\"ASSIGNED\">"));
    assertTrue("representedCustodian missing or wrong",
        asString.contains("<representedCustodianOrganization classCode=\"ORG\" determinerCode=\"INSTANCE\">"));

    assertTrue("id is missing or wrong",
        asString.contains("<id assigningAuthorityName=\"SOR\" extension=\"88878685\" root=\"1.2.208.176.1\"/>"));

    assertTrue("name is missing or wrong",
        asString.contains("<name>Odense Universitetshospital - Svendborg Sygehus</name>"));

    assertTrue("telecom is missing or wrong",
        asString.contains("<telecom use=\"WP\" value=\"tel:65223344\"/>"));

    assertTrue("custodian addr is missing or wrong",
        asString.contains("<streetAddressLine>Hjertemedicinsk afdeling B</streetAddressLine>"));
  }

  @Test
  public void shouldValidateLegalAuthenticator() {
    assertTrue("legalAuthenticator missing or wrong",
        asString.contains("<legalAuthenticator contextControlCode=\"OP\" typeCode=\"LA\">"));

    assertTrue("authentication time missing or wrong",
        asString.contains("<time value=\"20140113100000+0100\"/>"));

    assertTrue("signatureCode missing or wrong",
        asString.contains("<signatureCode nullFlavor=\"NI\"/>"));

    assertTrue("assignedEntity missing or wrong",
        asString.contains("<assignedEntity classCode=\"ASSIGNED\">"));

    assertTrue("assignedEntity address wrong or missing",
        asString.contains("<streetAddressLine>Hjertemedicinsk afdeling X</streetAddressLine>"));

    assertTrue("assignedEntity telecom wrong or missing",
        asString.contains("<telecom use=\"WP\" value=\"tel:55555555\"/>"));

    assertTrue("assignedEntity assignedPerson wrong or missing",
        asString.contains("<assignedPerson classCode=\"PSN\" determinerCode=\"INSTANCE\">"));

    assertTrue("assignedEntity assignedPersion given name wrong or missing",
        asString.contains("<given>Anders</given>"));
    assertTrue("assignedEntity assignedPersion family name wrong or missing",
        asString.contains("<family>Andersen</family>"));

    assertTrue("legalAuthenticator representedOrganization wrong or missing",
        asString.contains("<representedOrganization classCode=\"ORG\" determinerCode=\"INSTANCE\">"));
    assertTrue("legalAuthenticator representedOrganization name wrong or missing",
        asString.contains("<name>Odense Universitetshospital - Svendborg Sygehus X</name>"));
  }

  @Test
  public void shouldValidateDocumentationOf() {
    assertTrue("documentationOf wrong or missing",
        asString.contains("<documentationOf typeCode=\"DOC\">"));
    assertTrue("documentationOf serviceEvent wrong or missing",
        asString.contains("<serviceEvent classCode=\"MPROT\" moodCode=\"EVN\">"));
    assertTrue("documentationOf low wrong or missing",
        asString.contains("<low value=\"20140106080200+0100\"/>"));
    assertTrue("documentationOf high wrong or missing",
        asString.contains("<high value=\"20140110081500+0100\"/>"));

  }

  @Test
  public void shouldValidateWeightMeasurements() {
    assertTrue("Measurement section missing",
        asString.contains("<component contextConductionInd=\"true\" typeCode=\"COMP\">"));

    // validate weight no 1 correctly stored 
    assertEquals(NPU.DRY_BODY_WEIGHT_CODE, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("code", 0, "code", "observation", phmrAsXML)); 
    assertEquals(NPU.CODESYSTEM_OID, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystem", 0, "code", "observation", phmrAsXML)); 
    assertEquals("Legeme masse; Pt", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("displayName", 0, "code", "observation", phmrAsXML)); 
    assertEquals("NPU terminologien", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystemName", 0, "code", "observation", phmrAsXML)); 
    assertEquals("kg", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("unit", 0, "value", "observation", phmrAsXML)); 
    assertEquals("77.5", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("value", 0, "value", "observation", phmrAsXML));

    // validate weight no 2 correctly stored 
    assertEquals(NPU.DRY_BODY_WEIGHT_CODE, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("code", 1, "code", "observation", phmrAsXML)); 
    assertEquals(NPU.CODESYSTEM_OID, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystem", 1, "code", "observation", phmrAsXML)); 
    assertEquals("Legeme masse; Pt", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("displayName", 1, "code", "observation", phmrAsXML)); 
    assertEquals("NPU terminologien", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystemName", 1, "code", "observation", phmrAsXML)); 
    assertEquals("kg", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("unit", 1, "value", "observation", phmrAsXML)); 
    assertEquals("77.0", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("value", 1, "value", "observation", phmrAsXML));
  }

  @Test
  public void shouldValidateMedicalEquipment() {
    assertTrue("Medical equipment template 1 missing or wrong",
        asString.contains("<templateId root=\"2.16.840.1.113883.10.20.1.7\"/>"));
    assertTrue("Medical equipment template 2 missing or wrong",
        asString.contains("<templateId root=\"2.16.840.1.113883.3.4208.11.1\"/>"));

    assertTrue("Medical equipment manufacturer missing or wrong",
        asString.contains("<manufacturerModelName>Manufacturer: AD Company / Model: 6121ABT1</manufacturerModelName>"));
    assertTrue("Medical equipment softwarename missing or wrong",
        asString.contains("<softwareName>SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711</softwareName>"));

    assertTrue("Medical equipment code missing or wrong",
        asString.contains("<code code=\"EPQ12225\" codeSystem=\"2.16.840.1.113883.3.4208.100.7\" displayName=\"Weight\"/>"));
  }

  @Ignore
  @Test
  public void shouldAllowCreatingCDAInAnyOrder() {
    // Build a CDA in reverse order, and ensure the result is the same
    assertFalse(true);
  }

  @Ignore
  @Test
  public void shouldValidateMissingSections() {
    // Build a CDA but leave out sections to ensure validator is correct
    assertFalse(true);
  }

  @Test(expected = IllegalStateException.class)
  public void shouldThrowExceptionInCaseNoCodeSystemDefined() {
    Measurement.setCodeSystem(null, null);
    new NumericMeasurement("bimse", "nothing", "10.0", "kg", new Date(), true, null, null);
  }
}
