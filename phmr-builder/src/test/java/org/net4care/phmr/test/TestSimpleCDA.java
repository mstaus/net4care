package org.net4care.phmr.test;

import static org.junit.Assert.*;

import org.junit.*;
import org.net4care.phmr.builders.*;
import org.net4care.phmr.model.AddressData;
import org.net4care.phmr.model.OrganizationIdentity;
import org.net4care.phmr.model.PersonIdentity;
import org.net4care.phmr.model.SimpleClinicalDocument;
import org.net4care.phmr.model.Telecom;

/** Test-driven implementation of the validation logic on
 * a SimpleClinicalDocument instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class TestSimpleCDA {

  private SimpleClinicalDocument cda;
  private ValidatingBuilder validator;
  private OrganizationIdentity svendborgHjerteMedicinskAfdeling;
  private PersonIdentity wrong;

  @Before
  public void setup() {
    cda = SetupMedcomExample1.defineAsCDA();
    validator = new ValidatingBuilder();

    svendborgHjerteMedicinskAfdeling = 
        new OrganizationIdentity("88878685",
          "Odense Universitetshospital - Svendborg Sygehus",
          new Telecom[]{new Telecom(AddressData.Use.WorkPlace, "tel:65223344")},
          new AddressData(AddressData.Use.WorkPlace,
            new String[]{"Hjertemedicinsk afdeling B", "Valdemarsgade 53"},
            "5700", "Svendborg", "Danmark"));
  }

  /** Simple 'smoke test' of SimpleCDA's
   * toString method.
   */
  @Test
  public void shouldToStringSimpleCDA() {
    PlainStringBuilder builder = new PlainStringBuilder();
    cda.construct(builder);
    String result = builder.getResult(); 
    assertTrue(result.contains("SimpleClinicalDocument:"));
    assertTrue(result.contains("setId: 2358344 / version: 1"));
    assertTrue(result.contains("Patient: 2512484916: Nancy Berggren"));
    assertTrue(result.contains("Author:"));
    assertTrue(result.contains("- Anders Andersen"));
    assertTrue(result.contains("Odense Universitetshospital"));
    assertTrue(result.contains("0: Mon Jan 06 08:02:00 CET 2014 :: Legeme masse; Pt 77.5 kg"));
    assertTrue(result.contains("1: Wed Jan 08 07:45:00 CET 2014 :: Legeme masse; Pt 77.0 kg"));
    assertTrue(result.contains("Manufacturer: AD Company / Model: 6121ABT1 SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711"));
  }

  @Test
  public void shouldValidateCorrectSimpleCDA() {
    cda.construct(validator);
    assertTrue(validator.isValid());
  }

  @Test
  public void shouldDetectNullPatient() {
    cda.setPatient(null);
    cda.construct(validator);

    assertFalse("Missing a null patient check", validator.isValid());
    assertEquals("The patient identity is not set (null)", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyGivenNames1() {

    wrong = new PersonIdentity(new String[] {""}, "Hansen");
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The patient's first name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyGivenNames2() {
    wrong = new PersonIdentity(new String[] {}, "Hansen");
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The patient's first name is missing or empty", validator.errorList().get(0));

  }

  @Test
  public void shouldDetectMissingOrEmptyGivenNames3() {
    wrong = new PersonIdentity(null, "Hansen");
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The patient's first name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyFamilyName1() {
    wrong = new PersonIdentity(new String[] {"Benjamin"}, null);
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a last name check", validator.isValid());
    assertEquals("The patient's family name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyFamilyName2() {
    wrong = new PersonIdentity(new String[] {"Benjamin"}, "");
    cda.setPatient(wrong);
    cda.construct(validator);

    assertFalse("Missing a last name check", validator.isValid());
    assertEquals("The patient's family name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyAuthorName1() {
    // as the validation code is reused, less testing is needed to drive impl
    wrong = new PersonIdentity(new String[] {"Benjamin"}, null);
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, wrong, HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0));
    cda.construct(validator);

    assertFalse("Missing a last name check", validator.isValid());
    assertEquals("The author's family name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMissingOrEmptyAuthorName2() {
    wrong = new PersonIdentity(new String[] {""}, "Hansen");
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, wrong, HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0));
    cda.construct(validator);

    assertFalse("Missing a first name check", validator.isValid());
    assertEquals("The author's first name is missing or empty", validator.errorList().get(0));
  }

  @Test
  public void shouldDetectMultipleProblems() {
    wrong = new PersonIdentity(new String[] {""}, "Bergren");
    cda.setPatient(wrong);

    wrong = new PersonIdentity(new String[] {"Benjamin"}, "");
    cda.setAuthor(svendborgHjerteMedicinskAfdeling, wrong, HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0));
    cda.construct(validator);

    // System.out.println(validator.toString());

    assertEquals(2, validator.errorList().size());

  }
}
