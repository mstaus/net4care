package org.net4care.phmr.test;

import java.util.*;

import org.net4care.phmr.model.*;

public final class ValidatingBuilder implements PhmrBuilder {
  private List<String> errorList;
  private boolean isValid;

  public ValidatingBuilder() {
    errorList = new ArrayList<String>();
    isValid = true;
  }

  @Override
  public void buildRootNode() {
  }

  @Override
  public void buildHeader() {
  }

  @Override
  public void buildContext(PersonIdentity patientIdentity, Date effectiveTime,
      String setId, String versionNumber) {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildPatientSection(PersonIdentity patientIdentity) {
    checkIfNameIsWellformed(patientIdentity, "patient");
  }

  @Override
  public void buildResults(List<Measurement> results) {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildVitalSigns(List<Measurement> vitalSigns) {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildStructuredBodySection() {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildAuthorSection(
      OrganizationIdentity authorOrganizationIdentity,
      PersonIdentity authorIdentity, Date authorParticipationStartTime) {
    checkIfNameIsWellformed(authorIdentity, "author");
  }

  @Override
  public void buildCustodianSection(OrganizationIdentity custodianIdentity) {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildLegalAuthenticatorSection(
      OrganizationIdentity authenticatorOrganizationIdentity,
      PersonIdentity authenticatorPersonIdentity, Date timeOfAuthentication) {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildDocumentationOf(Date serviceStart, Date serviceEnd) {
    // TODO Auto-generated method stub
  }

  @Override
  public void buildMedicalEquipmentSection(List<MedicalEquipment> equipments) {
    // TODO Auto-generated method stub
  }

  public boolean isValid() {
    return isValid;
  }

  public List<String> errorList() {
    return errorList;
  }

  private void checkIfNameIsWellformed(PersonIdentity personIdentity, String personRoleName) {
    if (personIdentity == null) {
      errorList.add("The " + personRoleName + " identity is not set (null)");
      isValid = false; return;
    }
    if (personIdentity.getGivenNames() == null
        || personIdentity.getGivenNames().length == 0
        || personIdentity.getGivenNames()[0].equals("")) {
      errorList.add("The " + personRoleName + "'s first name is missing or empty");
      isValid = false; return;
    }
    if (personIdentity.getFamilyName() == null
        || personIdentity.getFamilyName().equals("")) {
      errorList.add("The " + personRoleName + "'s family name is missing or empty");
      isValid = false; return;
    }
  }

  public String toString() {
    String result = "Validity is: " + isValid + "\n";
    for (String line : errorList()) {
      result += " -> " + line + "\n";
    }
    return result;
  }
}
